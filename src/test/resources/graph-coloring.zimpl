# Standard Graph Coloring

set V;
set E;
set C;

var x[V*C] binary;
var w[C] binary;

# TODO: in ZimplVisitor: add 'objective' as name
minimize: sum <c> in C: w[c];
#minimize obj: sum <c> in C: w[c];

# TODO: in ZimplVisitor: add name labels
subto: forall <v> in V:
#subto c1: forall <v> in V:
	sum <c> in C: x[v,c] == 1;
subto: forall <u,v> in E: forall <c> in C:
#subto c2: forall <u,v> in E: forall <c> in C:
	x[u,c] + x[v,c] <= w[c];

