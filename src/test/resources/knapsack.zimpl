param C ;			# Capacity
param N ;			# Number of items
set I := { 1..N } ; # Indices
param w[I] ;		# Weight of item <i> in I
var x[I] binary ;	# decision variables
maximize : sum <i> in I : w[i]*x[i] ;
subto : sum <i> in I : w[i]*x[i] <= C ;
