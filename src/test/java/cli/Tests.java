package cli;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.Trees;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;

import tree.AST;
import tree.AstBuilder;
import zimpl.ZimplParser;
import zimpler.Resolver;
import zimpler.io.ZimplSTGenerator;
import zimpler.io.ZimplVisitor;

/** This tests must be run inside 'resources' testing directory, eg:
 * src/test/resources. 
 */
class Tests {
	
	@Test void classroom() throws IOException {
		AST model = new Resolver().resolveFromFilename("graph-coloring.zimpl");
		String result = model.accept(new ZimplVisitor());
		System.out.println("main/Tests/classroom().result:\n" + result);
		// TODO: test properly
	}
	
	@Test void knapsack() throws IOException {
		ZimplParser parser =
				ZimplParser.fromFilename("knapsack.zimpl");
		ParseTree tree = parser.formulation();
		String pattern = "<C:parameter>; <N:parameter>; <set>;" + 
				"<w:parameter>; <variable>; <objective>; <constraint>;";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		// FIXME: redesign this test. Checking undefined ids is not a bad idea.
//		Set<ParseTree> expected = 
//				Set.of(m.get("C"), m.get("N"), m.get("w"));
//		assertEquals(expected, Resolver.getUndefinedIds(tree));
	}

	@Test void testResolverResolve() {
		AST ast = AstBuilder.fromString("param P;");
		String result = ZimplSTGenerator.generate(ast);
		// FIXME: there's no "Resolver" nor "Resolve" here!
		assertEquals("param P;\n", result);
	}
	
	public static ParseTreeMatch 
	matchTree(Parser p, String pattern, ParseTree t) {
		int rule = getRootRuleIndex(p, t);
		return p.compileParseTreePattern(pattern, rule).match(t);
	}
	
	private static int getRootRuleIndex(Parser p, ParseTree t) {
		String rulename = Trees.getNodeText(t, p);
		Integer result = p.getRuleIndexMap().get(rulename);
		return result;
	}
	
}
