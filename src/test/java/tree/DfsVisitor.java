package tree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DfsVisitor extends AbstractVisitor<String>
implements Visitor<String> {
	
	public static String visitFromFilename(String filename)
	throws IOException {
		return visitFromFilename(filename, new DfsVisitor());
	}
	
	public static String visitFromString(String text) {
		return visitFromString(text, new DfsVisitor());
	}
	
	public String visitToken(AST it) {
		String text = it.token.getText();
		return text.equals("𝑭")? "" : text;
	}
	
	public String visitComparison(Comparison node) {
		return String.format("%s %s %s", fwd(node.getLhs()), 
				node.token.getText(), fwd(node.getRhs()));
	}
	
	public String visitOp(OpNode node) {
		List<String> childrenResults = new ArrayList<>();
		for (AST it : node.children)
			childrenResults.add(fwd(it));
		// NOTE: infix operator, for readability.
		String separator = node.token.getText();
		return String.join(separator, childrenResults);
	}
	
	@Override public String reduce(String current, String next, int i) {
		if (next == null)
			return current;
		if (current == null)
			return next;
		String sep = (current.isBlank() || current.endsWith("\n"))?
				"" : " ";
		return current + sep + next; 
	}

}
