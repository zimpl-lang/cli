package tree;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.io.IOException;

import zimpler.Resolver;
import zimpler.ResolverVisitor;
import zimpler.io.ZimplVisitor;

/**
 * These tests must be run in the src/test/resources dir.
 *  
 * @author Javier MV
 * @since 0.2
 */
class ResolverVisitorTests {
	
	Visitor<AST> visitor = new ResolverVisitor();
	
	@Test void knapsack() throws IOException {
		AST model = new Resolver().resolveFromFilename("knapsack.zimpl");
		String result = model.accept(new ZimplVisitor());
		String expected = """
				param C := 10;
				param N := 5;
				set I := {1..N};
				param w[I] := <1> 3, <2> 4, <3> 5, <4> 6, <5> 7;
				var x[I] binary;
				maximize: sum <i> in I: w[i]*x[i];
				subto: sum <i> in I: w[i]*x[i] <= C;
				""";
		assertEquals(expected, result);
	}
	
	@Test void emptyFormulation() {
		AST tree = AstBuilder.fromString("");
		AST result = tree.accept(visitor);
		assertEquals("𝑭", result.toStringTree());
	}
	
	// TODO: add (properties) data loading conditions tests: file
	// not found, not readable, not a file, duplicate keys...
	// TODO: add error conditions reporting capabilities to visitor
	// key not found, number or type mismatch... 
	
	@Test void setUndefinedResolvedEmpty() {
		AST tree = AstBuilder.fromString("set UE;");
		AST result = tree.accept(visitor);
		assertEquals("(𝑭 (set UE ({} ∅)))", result.toStringTree());
	}
	
	@Test void setUndefinedUnresolved() {
		AST tree = AstBuilder.fromString("set U;");
		AST result = tree.accept(visitor);
		assertEquals("(𝑭 (set U null))", result.toStringTree());
	}
	
	@Test void setDefinedSetExpr() {
		AST tree = AstBuilder.fromString("set D := P*Q;");
		AST result = tree.accept(visitor);
		assertEquals("(𝑭 (set D (* P Q)))", result.toStringTree());
	}
	
	@Test void setDefinedEmpty() {
		AST tree = AstBuilder.fromString("set E := { };");
		AST result = tree.accept(visitor);
		assertEquals("(𝑭 (set E ({} ∅)))", result.toStringTree());
	}
	
	// TODO: add tests for multiple parameters
	
	@Test void paramIndexedDefined() {
		AST tree = AstBuilder.fromString("param Q[J] := <3> 4;");
		AST result = tree.accept(visitor);
		// nulls are 'readfile' & (default) 'elem'.
		String expected =
				"(𝑭 (param Q J (⇶ null (⇉ (→ (<> 3) 4)) null)))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void paramIndexedUndefinedSize2() {
		AST tree = AstBuilder.fromString("param Q[K] ;");
		// 'mapping' is null
		assertEquals("(𝑭 (param Q K null))", tree.toStringTree());
		// TODO: it could help if mapping definitions could came from
		// a simple String, such as "Q[I] = <1> 2", without resorting
		// to reading file 'data.properties'.
		// current content of data.properties: Q[K] = <3> 4, <5> 6
		AST result = tree.accept(visitor);
		// nulls are 'readfile' & (default) 'elem'
		String expected = "(𝑭 (param Q K (⇶ null " +
				"(⇉ (→ (<> 3) 4) (→ (<> 5) 6)) null)))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void paramIndexedUndefinedSize1() {
		AST tree = AstBuilder.fromString("param Q[J] ;");
		// 'mapping' is null
		assertEquals("(𝑭 (param Q J null))", tree.toStringTree());
		// TODO: it could help if mapping definitions could came from
		// a simple String, such as "Q[I] = <1> 2", without resorting
		// to reading file 'data.properties'.
		// current content of data.properties: Q[J] = <1> 2
		AST result = tree.accept(visitor);
		// nulls are 'readfile' & (default) 'elem'
		String expected =
				"(𝑭 (param Q J (⇶ null (⇉ (→ (<> 1) 2)) null)))";
		assertEquals(expected, result.toStringTree());
	}
	
	// FIXME: Zimpl doesn't seem to restrict zero sized Mappings
//	@Test void paramIndexedUndefinedSize0() {
//		AST tree = AstBuilder.fromString("param Q[I] ;");
//		// 1st '?' stands for 'Formulation', 2nd for 'Parameters' (all
//		// of them); 'null' is for 'value'.
//		assertEquals("(? (? (param Q I null)))", tree.toStringTree());
//		// TODO: it could help if mapping definitions could came from
//		// a simple String, such as "Q[I] = <1> 2", without resorting
//		// to reading file 'data.properties'.
//		// current content of data.properties: "Q[I] = " (empty list) 
//		AST result = tree.accept(visitor);
//		System.out.println(result.toStringTree()); // "?"
//		String expected = "(? (? (param Q J (MAPPING (→ ) null))))";
//		assertEquals(expected, result.toStringTree());
//	}
	
	@Test void paramUnindexedDefined() {
		AST tree = AstBuilder.fromString("param D := 1 ;");
		String expected = "(𝑭 (param D null 1))"; // 'index' is null
		assertEquals(expected, tree.toStringTree());
		AST result = tree.accept(visitor);
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void paramUnindexedUndefinedResolvable() {
		AST tree  = AstBuilder.fromString("param R ;");
		// nulls stand for 'index' & 'elem'.
		assertEquals("(𝑭 (param R null null))", tree.toStringTree());
		AST result = tree.accept(visitor);
//		System.out.println(result.toStringTree());
		assertEquals("(𝑭 (param R null 0))", result.toStringTree());
	}
	
	@Test void paramUnindexedUndefinedUnresolvable() {
		AST tree  = AstBuilder.fromString("param U ;");
		AST result = tree.accept(visitor);
		assertEquals(tree.toStringTree(), result.toStringTree());
	}
	
}
