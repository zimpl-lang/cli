package tree;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

/** These tests must be run in the src/test/resources dir. */
class NamedProblemsTests {

	Visitor<String> visitor = new DfsVisitor();

	@Test void lotsizing() throws IOException {
		AST tree = AstBuilder.fromFilename("lot-sizing.zimpl");
		String result = tree.accept(visitor);
		String expected =
				"param N " +
				"set T {} .. 1 N " +
				"set W {} .. 0 N " +
				"param d T " +
				"param c T " +
				"param cs " +
				"param maxx " +
				"param maxs " +
				"param mins " +
				"param s0 " +
				"var x T >= 0 " +
				"var s W >= 0 " +
				"minimize cost sum in <> t T : c t*x t+cs*s t " +
				"subto defstock forall in <> t T : " +
					"s t == s t-1+x t-d t " +
				"subto maxprod forall in <> t T : x t <= maxx " +
				"subto maxstock forall in <> t T : s t <= maxs " +
				"subto minstock forall in <> t T : s t >= mins " +
				"subto initstock s 0 == s0";
		assertEquals(expected, result);
	}
	
	@Test void knapsack() throws IOException {
		AST tree = AstBuilder.fromFilename("knapsack.zimpl");
		String result = tree.accept(visitor);
		String expected =
				"param C " +
				"param N " +
				"set I {} .. 1 N " +
				"param w I " +
				"var x I binary " +
				"maximize sum in <> i I : w i*x i " +
				"subto sum in <> i I : w i*x i <= C";
		assertEquals(expected, result);
	}
	
}
