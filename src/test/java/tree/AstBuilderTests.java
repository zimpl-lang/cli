package tree;

import static org.junit.jupiter.api.Assertions.*;
import static zimpl.ZimplParser.*;
import static tree.AstBuilder.fromString;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class AstBuilderTests {
	
	@Test void emptyFormulation() {
		AST result = fromString("");
		assertEquals("𝑭", result.toStringTree());
	}
	
	@Test void functionDefSet() {
		AST result = fromString("defset w(c) := I ;");
//		System.out.println(result.toStringTree());
		String expected = "(𝑭 (𝑓 defset w c I))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void functionDeclBoolArgMany() {
		AST result = fromString("defbool h(b, c) ;");
		// ',' stands for 'csv', null for 'expr'
		String expected = "(𝑭 (𝑓 defbool h (, b c) null))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void functionDeclStrgArgSingle() {
		AST result = fromString("defstrg g(a) ;");
		String expected = "(𝑭 (𝑓 defstrg g a null))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void functionDeclNumbArgNone() {
//		function : FN_TYPE name=ID '(' csv ')' (':=' xExpr)? ;
		// NOTE: 'expr' should be renamed 'nExpr' for Numeric Expr; then, xExpr
		// should be renamed as 'Expr'
//		xExpr : setExpr | expr | boolExpr | STRING ;
//		FN_TYPE : 'defnumb' | 'defstrg' | 'defbool' | 'defset' ;
		AST result = fromString("defnumb f() ;");
		// ',' is for (empty) 'csv', 'null' for expr
		String expected = "(𝑭 (𝑓 defnumb f , null))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void constraintNamed() {
		AST result = fromString("subto c : 1 <= 2 ;");
//		System.err.println(result.toStringTree());
		String expected = "(𝑭 (subto c null (<= 1 2)))"; // forall is null
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void constraintUnnamed() {
//		constraint: 'subto' name=ID? ':' forall* comparison ;
		AST result = fromString("subto : 0 != 1 ;");
		// name & forall are null
		String expected = "(𝑭 (subto null null (!= 0 1)))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void objMaxNamed() {
//		objective : GOAL name=ID? ':' expr;
//		GOAL	  : 'minimize' | 'maximize' ;
		AST result = fromString("maximize obj : 4 ;");
		String expected = "(𝑭 (maximize obj 4))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void objMinUnnamed() {
		AST result = fromString("minimize : 3 ;");
//		System.out.println(result.toStringTree());
		String expected = "(𝑭 (minimize null 3))"; // name is null
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void objMinimizeNamed() {
		AST result = fromString("minimize obj : 1;");
		String expected = "(𝑭 (minimize obj 1))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void objMaximizeUnnamed() {
//		objective : GOAL name=ID? ':' expr;
//		GOAL	  : 'minimize' | 'maximize' ;
		AST result = fromString("maximize: 0;");
		String expected = "(𝑭 (maximize null 0))"; // name is null
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varIndexedTypedBoundDouble() {
//		variable: 'var' name=ID ('[' index ']')? TYPE? b1=bound? b2=bound? ;
//		index	: condition | ID ;
//		TYPE	: 'real' | 'binary' | 'integer' ;
//		bound	: cmp ( expr | sign=('+'|'-')? INFINITY ) ;
//		cmp		:	token=( '<' | '<=' | '!=' | '==' | '>=' | '>' ) ;
		AST result = fromString("var w[K] real > -infinity <= 1;");
//		String expected = "(𝑭 (var w K real (> (∞ -)) (<= 1)))";
		String expected = "(𝑭 (var w K real (> (- infinity)) (<= 1)))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varIndexedTypedBoundSingle() {
		AST result = fromString("var z[J] binary == 0;");
		// null is for 2nd 'bound'
		String expected = "(𝑭 (var z J binary (== 0) null))";
//		System.out.println(result.toStringTree());
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varIndexedTypedUnbound() {
		AST result = fromString("var x[I] integer;");
		// nulls are 'bound's
		String expected = "(𝑭 (var x I integer null null))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varIndexedUntypedBoundDouble() {
		AST result = fromString("var w[K] < +infinity > 0 ;");
		// 'type' is null
//		String expected = "(𝑭 (var w K null (< (∞ +)) (> 0)))";
		String expected = "(𝑭 (var w K null (< (+ infinity)) (> 0)))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varIndexedUntypedBoundSingle() {
		AST result = fromString("var z[J] <= 1;");
		// nulls are 'type' & (extra) bound
		String expected = "(𝑭 (var z J null (<= 1) null))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varIndexedUntypedUnbound() {
		AST result = fromString("var y[I];");
		// nulls are 'type' & 2x 'bound'
		String expected = "(𝑭 (var y I null null null))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varUnindexedTypedUnbound() {
		AST result = fromString("var x binary;");
		// 1st 'null' is for 'index', 2nd & 3rd for bounds
		String expected = "(𝑭 (var x null binary null null))";
//		System.out.println(result.toStringTree());
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varUnindexedUntypedBoundDouble() {
		AST result = fromString("var z <= 0 > -infinity;");
		// 'nulls' are for 'index' & 'type'
//		String expected = "(𝑭 (var z null null (<= 0) (> (∞ -))))";
		String expected = "(𝑭 (var z null null (<= 0) (> (- infinity))))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varUnindexedUntypedBoundSingle() {
		AST result = fromString("var y >= 0 ;");
		// 'nulls' stand for 'index', 'type' & 2nd bound.
		String expected = "(𝑭 (var y null null (>= 0) null))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void varUnindexedUntypedUnbound() {
//		variable: 'var' name=ID ('[' index ']')? TYPE? b1=bound? b2=bound? ;
//		index	: condition | ID ;
//		TYPE	: 'real' | 'binary' | 'integer' ;
//		bound	: cmp ( expr | sign=('+'|'-')? INFINITY ) ;
//		cmp		:	token=( '<' | '<=' | '!=' | '==' | '>=' | '>' ) ;
		AST result = fromString("var x ;");
		// 'nulls' stand for 'index', 'type' & 2 bounds
		String expected = "(𝑭 (var x null null null null))";
		assertEquals(expected, result.toStringTree());
	}
	
	// TODO: add tests for { set, param }:= readfile
	// TODO: add tests for mapping
	// TODO: add tests for multiple statements: parameters, sets?, ...
	
	@Test void paramIndexedDefinedSize2() {
		AST result = fromString("param Q[K] := <1> 2, <3> 4;");
		// nulls are 'readfile' & (default) 'elem' from 'mapping'
		String expected = "(𝑭 (param Q K " +
				"(⇶ null (⇉ (→ (<> 1) 2) (→ (<> 3) 4)) null)))";
//		System.err.println(result.toStringTree());
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void paramIndexedDefinedSize1() {
//		parameter : 'param' name=ID ( (':=' elem )? | ':=' readfile )
//			| '[' index ']' ( ':=' mapping )? ;
//		mapping : ( readfile | chain | readfile ',' chain) ('default' elem)? ;
		AST result = fromString("param Q[J] := <1> 2;");
		// nulls are 'readfile' & (default) 'elem' from 'mapping'
		String expected =
				"(𝑭 (param Q J (⇶ null (⇉ (→ (<> 1) 2)) null)))";
		assertEquals(expected, result.toStringTree());
	}
	
	// FIXME: Zimpl doesn't seem to restrict zero sized Mappings
//	@Test void paramIndexedDefinedSize0() {
//		AST result = fromString("param Q[I] := ;");
//		// 1st '?' stands for 'Formulation', 2nd for 'Parameters'
//		// (all of them); 'null' is for 'default value' of Mapping 
//		String expected = "(? (? (param Q J (MAPPING (→) null))))";
//		System.err.println(result.toStringTree());
//		assertEquals(expected, result.toStringTree());
//	}
	
	@Test void paramIndexedUndefined() {
		AST result = fromString("param Q[U] ;");
		// 'elem' is null
		assertEquals("(𝑭 (param Q U null))", result.toStringTree());
	}
	
	@Test void paramUnindexedDefined() {
		AST result = fromString("param D := 1 ;");
		// 'null' is for 'index'.
		assertEquals("(𝑭 (param D null 1))", result.toStringTree());
	}
	
	@Test void paramUnindexedUndefined() {
//		parameter : 'param' name=ID ( (':=' elem )? | ':=' readfile )
//			| '[' index ']' ( ':=' mapping )? ;
		AST result = fromString("param U ;");
		// nulls stand for 'index' & 'elem' (value)
		assertEquals("(𝑭 (param U null null))", result.toStringTree());
	}
	
	@Test void setRangeMix() {
		AST result = fromString("set RM := { 0..N } ;");
		String expected = "(𝑭 (set RM ({} (.. 0 N))))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void setRangeId() {
		AST result = fromString("set RD := { I..J } ;");
		String expected = "(𝑭 (set RD ({} (.. I J))))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void setRangeInt() {
		AST result = fromString("set RI := { 0..1 } ;");
		assertEquals("(𝑭 (set RI ({} (.. 0 1))))", result.toStringTree());
//		System.out.println(result.toStringTree());
	}
	
	@Test void setEmpty() {
		AST result = fromString("set E := { } ;");
		assertEquals("(𝑭 (set E ({} ∅)))", result.toStringTree());
	}
	
	// non-statement rules ---------------------------------------------
	
	@Disabled
	@Test void forallForall() {
		String text = "forall <i> in I : forall <j> in J: i > j";
		AST result = fromString(text, RULE_forall);
		String expected = "(forall (in (<> i) I null) " +
				"(forall (in (<> j) J null) (> i j)))";
		String actual = "(forall (in (<> i) I null) : null)";
//		assertEquals(expected, result.toStringTree());
		assertEquals(actual, result.toStringTree());
	}
	
	@Test void forallColon() {
		// TODO: see if this condition is now tested elsewhere (constraint?)
//		AST result = fromString("forall <c> in C : 1 < c", RULE_forall);
		// nulls are (guard) separator, boolExpr & (nested) forall 
		AST result = fromString("forall <c> in C :", RULE_forall);
		String expected = "(forall (in (<> c) C null null) : null)";
//		String expected = "(forall (in (<> c) C null) (< 1 c))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void forallDo() {
//		AST result = fromString("forall <d> in D do d > 0", RULE_forall);
		AST result = fromString("forall <d> in D do", RULE_forall);
//		// 1st null is for (guard) 'boolExpr', 2nd for (nested) forall
		// nulls are (guard) separator, boolExpr & (nested) forall 
		String expected = "(forall (in (<> d) D null null) do null)";
//		// null is for (guard) 'boolExpr'
//		String expected = "(forall (in (<> d) D null) (> d 0))";
		assertEquals(expected, result.toStringTree());
	}
	
	@Disabled
	@Test void forallComparison() {
////	condition 	: tuple 'in' setExpr (('with' | '|') boolExpr)? ;
//	//	forall: 'forall' condition sep=('do' | ':') forall	# ForallNested
//	//			|											# ForallEmpty
//				;
//		forall	: 'forall' condition sep=('do' | ':') ;
		AST result = fromString("1 >= 0", RULE_forall);
		assertEquals("(>= 1 0)", result.toStringTree());
	}
	
	// FIXME: add boolExpr tests
	
	// TODO: implement in AstBuilder
	@Test @Disabled
	void comparison2LT3LE4() {
		AST result = fromString("2 < 3 <= 4", RULE_comparison);
		System.out.println(result.toStringTree());
	}
	
	@Test void comparison0NE1() {
//		comparison 	: expr bound+ ;
//		bound		: cmp ( expr | sign=('+'|'-')? INFINITY ) ;
//		cmp			: '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		AST result = fromString("0 != 1", RULE_comparison);
		assertEquals("(!= 0 1)", result.toStringTree());
	}
	
	@Test void boundInfinitySigned() {
		AST result = fromString("< +infinity", RULE_bound);
//		assertEquals("(< (∞ +))", result.toStringTree());
		assertEquals("(< (+ infinity))", result.toStringTree());
	}
	
	@Test void boundInfinityUnsigned() {
		AST result = fromString("<= infinity", RULE_bound);
//		assertEquals("(<= ∞)", result.toStringTree());
		assertEquals("(<= infinity)", result.toStringTree());
	}
	
	@Test void boundExpr() {
//		bound		: cmp ( expr | sign=('+'|'-')? INFINITY ) ;
//		cmp			: '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		AST result = fromString("== 0", RULE_bound);
//		System.out.println(result.toStringTree());
		assertEquals("(== 0)", result.toStringTree());
	}
	
	@Test void exprUnsignedPrecedence() {
		AST result = fromString("1+2*3", RULE_nExpr);
		assertEquals("(+ 1 (* 2 3))", result.toStringTree());
	}
	
	@Test void exprUnsignedAssociativity() {
		AST result = fromString("4+5-6", RULE_nExpr);
		assertEquals("(- (+ 4 5) 6)", result.toStringTree());
	}
	
	@Test void exprUnsignedBinOp() {
		AST result = fromString("2*3", RULE_nExpr);
		assertEquals("(* 2 3)", result.toStringTree());
	}
	
	@Test void exprSignedBinOp() {
//		AST result = fromString("-1+2", RULE_expr);
//		assertEquals("(+ (- 1) 2)", result.toStringTree());
		// FIXME: see AstBuilder.exitExpr(). A Visitor seems like a
		// better fix than children fiddling.
		// TODO: though semantically incorrect, this is perfectly fine
		// from a syntactic view
		Exception ex = assertThrows(RuntimeException.class, () ->
				fromString("-1+2", RULE_nExpr)
			);
		String msg = "Child type 22 not currently supported in phrase: " +
			"(+ 1 2)";
		assertEquals(msg, ex.getMessage());
	}
	
	@Test void exprSignedBasicMinus() {
		AST result = fromString("-0.5", RULE_nExpr);
		assertEquals("(- 0.5)", result.toStringTree());
	}
	
	@Test void exprSignedBasicPlus() {
//		nExpr	:	sign=('+'|'-')? uExpr ;
//		uExpr	:	uExpr ('*'|'/'|'+'|'-') uExpr | basicExpr ;
		AST result = fromString("+1", RULE_nExpr);
		assertEquals("(+ 1)", result.toStringTree());
	}
	
	// TODO: add tests for sumExpr, condition, fnRef
//	sumExpression : 'sum' condition ('do' | ':') expr ;
//	condition : tuple 'in' setExpr (('with' | '|') boolExpr)? ;
//	fnRef: name=ID '(' args ')' ;
//	args: (arg (',' arg)*)? ;
//	arg	: expr | STRING ;
	
	@Test void basicExprParen() {
		AST result = fromString("(0)", RULE_basicExpr);
		assertEquals("(() 0)", result.toStringTree());
	}
	
	@Test void basicExprFnRef() {
//		basicExpr:	sumExpression | fnRef | '(' expr ')' | ...
		AST result = fromString("f()", RULE_basicExpr);
		assertEquals("(f ,)", result.toStringTree());
	}
	
	@Test void basicExprSumExpr() {
//		sumExpression: 'sum' condition ('do' | ':') expr ;
//		condition 	 : tuple 'in' setExpr (('with' | '|') boolExpr)? ;
		AST result = fromString("sum <i> in I : 2", RULE_basicExpr);
		// nulls are (guard) separator & boolExpr
		String expected = "(sum (in (<> i) I null null) : 2)";
		assertEquals(expected, result.toStringTree());
	}
	
	@Test void basicExprFloat() {
		AST result = fromString("3.1", RULE_basicExpr);
		assertEquals("3.1", result.toStringTree());
	}
	
	@Test void basicExprInt() {
		AST result = fromString("2", RULE_basicExpr);
//		System.out.println(result.toStringTree());
		assertEquals("2", result.toStringTree());
	}
	
	@Test void basicExprIdIdxMany() {
		AST result = fromString("K[0,1]", RULE_basicExpr);
		assertEquals("(K (, 0 1))", result.toStringTree());
	}
	
	@Test void basicExprIdIdxSingle() {
		AST result = fromString("J[j]", RULE_basicExpr);
//		System.out.println(result.toStringTree());
		assertEquals("(J j)", result.toStringTree());
	}
	
	@Test void basicExprIdIdxNone() {
//		basicExpr:	ID ('[' csv ']')? | INT | FLOAT | ...
		AST result = fromString("I", RULE_basicExpr);
		assertEquals("I", result.toStringTree());
	}
	
	// TODO: add tests for remaining non-statement rules
	
}
