package tree;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DfsVisitorTests {

	Visitor<String> visitor = new DfsVisitor();

	@Test void emptyFormulation() {
		String result = DfsVisitor.visitFromString("");
		assertEquals("", result);
	}
	
	@Test void constraintForallSingle() {
		String text = "subto : forall <c> in C do x[c] > 0 ;";
		String result = DfsVisitor.visitFromString(text);
//		System.out.println(result);
		assertEquals("subto forall in <> c C do x c > 0", result);
	}
	
	@Test void constraintMinimalNamed() {
		String text = "subto c : x[i] <= 1.0 ;";
		String result = DfsVisitor.visitFromString(text);
		assertEquals("subto c x i <= 1.0", result);
	}

	@Test void constraintMinimalUnnamed() {
//		constraint :  'subto' ID? ':' comparison ;
//		comparison : expression cmp expression ( cmp expression ) ? ;
//		cmp : '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		String text = "subto : 0 == 0 ;";
		String result = DfsVisitor.visitFromString(text);
		assertEquals("subto 0 == 0", result);
	}

	@Test void objSumProduct() {
		// objective : ('minimize' | 'maximize') ID? ':' expression;
		String text = "maximize obj : sum <j> in J : 2*x[j] ;";
		String result = DfsVisitor.visitFromString(text);
		assertEquals("maximize obj sum in <> j J : 2*x j", result);
	}

	@Test void objSumInt() {
		String text = "maximize obj : sum <i> in I : 1 ;";
		String result = DfsVisitor.visitFromString(text);
		assertEquals("maximize obj sum in <> i I : 1", result);
	}
	
	@Test void objProductIndexed() {
		String text = "maximize obj : 2*x[i];";
		String result = DfsVisitor.visitFromString(text);
		assertEquals("maximize obj 2*x i", result);
	}

	@Test void objProductSimple() {
		String text = "maximize obj : 2 * x;";
		String result = DfsVisitor.visitFromString(text);
		assertEquals("maximize obj 2*x", result);
	}
	
	@Test void objParentesis() {
		String text = "minimize obj : (2);";
		String result = DfsVisitor.visitFromString(text);
		assertEquals("minimize obj () 2", result);
	}

	@Test void objMinimalNamed() {
		String result = DfsVisitor.visitFromString("maximize obj : 1;");
		assertEquals("maximize obj 1", result);
	}

	@Test void objMinimalUnnamed() {
		// objective : ('minimize' | 'maximize') ID? ':' expression;
		String result = DfsVisitor.visitFromString("minimize : 0;");
		assertEquals("minimize 0", result);
	}

	@Test void varIndexedTypedBoundSingle() {
		AST tree = AstBuilder.fromString("var x[I] real == 0;");
		String result = tree.accept(visitor);
		assertEquals("var x I real == 0", result);
	}

	@Test void varIndexedTypedUnbound() {
		AST tree = AstBuilder.fromString("var x[I] integer;");
		String result = tree.accept(visitor);
		assertEquals("var x I integer", result);
	}

	// TODO: add tests for varIndexedUntypedBoundDouble() and (signed) infinities 

	@Test void varIndexedUntypedBoundSingle() {
		AST tree = AstBuilder.fromString("var x[I] <= 1;");
		String result = tree.accept(visitor);
		assertEquals("var x I <= 1", result);
	}

	@Test void varIndexedUntypedUnbound() {
		AST tree = AstBuilder.fromString("var x[I];");
		String result = tree.accept(visitor);
		assertEquals("var x I", result);
	}

	@Test void varSingleTypedUnbound() {
		AST tree = AstBuilder.fromString("var x binary;");
		String result = tree.accept(visitor);
		assertEquals("var x binary", result);
	}
	
	// TODO: add tests for varSingleUntypedBoundDouble() and (signed) infinities 
	
	@Test void varSingleUntypedBoundSingle() {
		AST tree = AstBuilder.fromString("var x >= 0 ;");
		String result = tree.accept(visitor);
		assertEquals("var x >= 0", result);
	}
	
	@Test void varSingleUntypedUnbound() {
		AST tree = AstBuilder.fromString("var x;");
		String result = tree.accept(visitor);
		assertEquals("var x", result);
	}

	// TODO: add tests for multiple parameters
	
	@Test void paramIndexedDefinedSize2() {
		AST tree = AstBuilder.fromString("param Q[K] := <1> 2, <3> 4;");
		String result = tree.accept(visitor);
		assertEquals("param Q K ⇶ ⇉ → <> 1 2 → <> 3 4", result);
	}
	
	@Test void paramIndexedDefinedSize1() {
		AST tree = AstBuilder.fromString("param Q[J] := <1> 2;");
		String result = tree.accept(visitor);
		assertEquals("param Q J ⇶ ⇉ → <> 1 2", result);
	}
	
	// FIXME: Zimpl doesn't seem to restrict zero sized Mappings
//	@Test void paramIndexedDefinedSize0() {
//		AST tree = AstBuilder.fromString("param Q[I] := ;");
//		String result = tree.accept(visitor);
//		System.err.println(result);
//		assertEquals("param Q I MAPPING →", result);
//	}
	
	@Test void paramIndexedUndefined() {
		AST tree = AstBuilder.fromString("param Q[U] ;");
		String result = tree.accept(visitor);
//		System.out.println(result);
		assertEquals("param Q U", result);
	}
	
	@Test void paramUnindexedDefined() {
		AST tree = AstBuilder.fromString("param D := 1 ;");
		String result = tree.accept(visitor);
		assertEquals("param D 1", result);
	}

	@Test void paramUnindexedUndefined() {
		AST tree = AstBuilder.fromString("param U ;");
		String result = tree.accept(visitor);
		assertEquals("param U", result);
	}
	
	@Test void setRangeMix() {
		AST tree = AstBuilder.fromString("set RM := { 0..N } ;");
		String result = tree.accept(visitor);
		assertEquals("set RM {} .. 0 N", result);
	}

	@Test void setRangeId() {
		AST tree = AstBuilder.fromString("set RD := { I..J } ;");
		String result = tree.accept(visitor);
		assertEquals("set RD {} .. I J", result);
	}

	@Test void setRangeInt() {
		AST tree = AstBuilder.fromString("set RI := { 0..1 } ;");
		String result = tree.accept(visitor);
		assertEquals("set RI {} .. 0 1", result);
	}

	@Test void setEmpty() {
		AST tree = AstBuilder.fromString("set E := { } ;");
		String result = tree.accept(visitor);
		assertEquals("set E {} ∅", result);
	}
}
