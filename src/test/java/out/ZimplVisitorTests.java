package out;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import tree.AST;
import tree.AstBuilder;
import tree.Visitor;
import zimpler.io.ZimplVisitor;

/** This tests must be run inside 'resources' testing directory, eg:
 * src/test/resources. 
 */
class ZimplVisitorTests {
	
	Visitor<String> visitor = new ZimplVisitor();
	
	@Test void knapsack() throws IOException {
		AST tree = AstBuilder.fromFilename("knapsack.zimpl");
		String result = tree.accept(visitor);
		String expected = """
				param C;
				param N;
				set I := {1..N};
				param w[I];
				var x[I] binary;
				maximize: sum <i> in I: w[i]*x[i];
				subto: sum <i> in I: w[i]*x[i] <= C;
				""";
		assertEquals(expected, result);
	}

	@Test void formulationEmpty() {
		String result = ZimplVisitor.visitFromString("");
//		System.out.println("out: '" + result + "'");
		assertEquals("", result);
	}

	@Test void constraintMinimalNamed() {
		String text = "subto c : x[i] <= 1 ;";
		String result = ZimplVisitor.visitFromString(text);
		assertEquals("subto c: x[i] <= 1;\n", result);
	}
	
	@Test void constraintMinimalUnnamed() {
//		constraint :  'subto' ID? ':' comparison ;
//		comparison : expression cmp expression ( cmp expression ) ? ;
//		cmp : '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		String text = "subto : 0 == 0 ;";
//		AST tree = AstBuilder.fromString(text);
//		System.out.println(ast.toStringTree());
//		String result = tree.accept(visitor);
		String result = ZimplVisitor.visitFromString(text);
		assertEquals("subto: 0 == 0;\n", result);
	}
	
	@Test void objSumProduct() {
		// objective : ('minimize' | 'maximize') ID? ':' expression;
		String text = "maximize obj : sum <j> in J : 2*x[j] ;";
		String result = ZimplVisitor.visitFromString(text);
		assertEquals("maximize obj: sum <j> in J: 2*x[j];\n", result);
	}

	@Test void objSumInt() {
		String text = "maximize : sum <i> in I: 1 ;";
		String result = ZimplVisitor.visitFromString(text);
		assertEquals("maximize: sum <i> in I: 1;\n", result);
	}
	
	@Test void objProductIndexed() {
		AST tree = AstBuilder.fromString("maximize : 2 * x[i];");
		String result = tree.accept(visitor);
		assertEquals("maximize: 2*x[i];\n", result);
	}

	@Test void objProductSimple() {
		AST tree = AstBuilder.fromString("maximize : 2 * x;");
		String result = tree.accept(visitor);
		assertEquals("maximize: 2*x;\n", result);
	}

	@Test void objParentesis() {
		AST tree = AstBuilder.fromString("minimize : ( 2 );");
		String result = tree.accept(visitor);
		assertEquals("minimize: (2);\n", result);
	}
	
	@Test void objMinimalNamed() {
		AST tree = AstBuilder.fromString("maximize obj : 1;");
		String result = tree.accept(visitor);
		assertEquals("maximize obj: 1;\n", result);
	}
	
	@Test void objMinimalUnnamed() {
//		// objective : ('minimize' | 'maximize') ID? ':' expression;
		AST tree = AstBuilder.fromString("minimize : 0;");
		String result = tree.accept(visitor);
		assertEquals("minimize: 0;\n", result);
	}
	
	@Test void varDouble() {
		String text = "var x; var y;";
		String result = ZimplVisitor.visitFromString(text);
		assertEquals("var x;\n" + "var y;\n", result);
	}
	
	@Test void varIndexedTypedBoundSingle() {
		AST tree = AstBuilder.fromString("var x[I] real == 0;");
		String result = tree.accept(visitor);
//		System.out.println("zpl: " + result);
		assertEquals("var x[I] real == 0;\n", result);
	}
	
	@Test void varIndexedTypedUnbound() {
		AST tree = AstBuilder.fromString("var x[I] integer;");
		String result = tree.accept(visitor);
		assertEquals("var x[I] integer;\n", result);
	}
	
	// TODO: add tests for varIndexedUntypedBoundDouble() and (signed) infinities 

	@Test void varIndexedUntypedBoundSingle() {
		AST tree = AstBuilder.fromString("var x[I] <= 1;");
		String result = tree.accept(visitor);
//		System.out.println("zpl: " + result);
		assertEquals("var x[I] <= 1;\n", result);
	}

	@Test void varIndexedUntypedUnbound() {
		AST tree = AstBuilder.fromString("var x[I];");
		String result = tree.accept(visitor);
		assertEquals("var x[I];\n", result);
	}
	
	@Test void varSingleTypedUnbound() {
		AST tree = AstBuilder.fromString("var x binary;");
		String result = tree.accept(visitor);
		assertEquals("var x binary;\n", result);
	}
	
	@Test void varSingleUntypedBoundSingle() {
		AST tree = AstBuilder.fromString("var x >= 0 ;");
		String result = tree.accept(visitor);
		assertEquals("var x >= 0;\n", result);
	}
	
	@Test void varSingleUntypedUnbound() {
		AST tree = AstBuilder.fromString("var x ;");
		String result = tree.accept(visitor); 
		assertEquals("var x;\n", result);
	}
	
	@Test void paramDouble() {
		String text = "param P:=0; param Q:=1;";
		String result = ZimplVisitor.visitFromString(text);
		String expected = "param P := 0;\n" + "param Q := 1;\n" ;
		assertEquals(expected, result);
	}

	@Test void paramIndexedDefinedSize2() {
		String text = "param Q[K] := <1> 2, <3> 4 ;";
		AST tree = AstBuilder.fromString(text);
		String result = tree.accept(visitor);
		assertEquals("param Q[K] := <1> 2, <3> 4;\n", result);
	}
	
	@Test void paramIndexedDefinedSize1() {
		AST tree = AstBuilder.fromString("param Q[J] := <1> 2 ;");
		String result = tree.accept(visitor);
		assertEquals("param Q[J] := <1> 2;\n", result);
	}

	// FIXME: Zimpl doesn't seem to restrict zero sized Mappings
//	@Test void paramIndexedDefinedSize0() {
//		AST tree = AstBuilder.fromString("param Q[I] := ;");
//		System.out.println(tree.toStringTree());
//		String result = tree.accept(visitor);
//		System.err.println(result);
//		assertEquals("param Q[J] := ;\n", result);
//	}

	@Test void paramIndexedUndefined() {
		String result = ZimplVisitor.visitFromString("param Q[U] ;");
		assertEquals("param Q[U];\n", result);
	}
	
	@Test void paramUnindexedDefined() {
		String result = ZimplVisitor.visitFromString("param D := 1 ;");
		assertEquals("param D := 1;\n", result);
	}
	
	@Test void paramUnindexedUndefined() {
		String result = ZimplVisitor.visitFromString("param U ;");
		assertEquals("param U;\n", result);
	}

	@Test void setRangeMix() {
		AST tree = AstBuilder.fromString("set RM := { 0..N } ;");
		String result = tree.accept(visitor);
		assertEquals("set RM := {0..N};\n", result);
	}
	
	@Test void setRangeId() {
		AST tree = AstBuilder.fromString("set RD := { I..J } ;");
		String result = tree.accept(visitor);
		assertEquals("set RD := {I..J};\n", result);
	}
	
	@Test void setRangeInt() {
		AST tree = AstBuilder.fromString("set RI := { 0..1 } ;");
		String result = tree.accept(visitor);
		assertEquals("set RI := {0..1};\n", result);
	}
	
	@Test void setEmpty() {
//		: 'set' name=ID ':=' value=setExpression ;
		AST tree = AstBuilder.fromString("set E := { } ;");
//		System.out.println(tree.toStringTree());
		String result = tree.accept(visitor);
		assertEquals("set E := { };\n", result);
	}
	
	@Test void setUndefined() {
		AST tree = AstBuilder.fromString("set U ;");
		String result = tree.accept(visitor);
		assertEquals("set U;\n", result);
	}
}
