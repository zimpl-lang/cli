package out;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import tree.AST;
import tree.AstBuilder;
import zimpler.io.ZimplSTGenerator;

/** This tests must be run inside 'resources' testing directory, eg:
 * src/test/resources. 
 */
class ZimplSTGTests {
	
	@Test void knapsack() throws IOException {
		String result = ZimplSTGenerator.fromFilename("knapsack.zimpl");
		String expected = """
				param C;
				param N;
				set I := {1..N};
				param w[I];
				var x[I] binary;
				maximize : sum <i> in I: w[i]*x[i];
				subto : sum <i> in I: w[i]*x[i] <= C;
				""";
		assertEquals(expected, result);
	}

	@Test void formulationEmpty() {
		AST ast = AstBuilder.fromString("");
		String result = ZimplSTGenerator.generate(ast);
//		System.out.println("out: '" + result + "'");
		assertEquals("", result);
	}

	@Test void constraintMinimalNamed() {
		String text = "subto c : x[i] <= 1 ;";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("subto c : x[i] <= 1;\n", result);
	}
	
	@Test void constraintMinimalUnnamed() {
//		constraint :  'subto' ID? ':' comparison ;
//		comparison : expression cmp expression ( cmp expression ) ? ;
//		cmp : '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		String text = "subto : 0 == 0 ;";
//		AST ast = AstBuilder.fromString(text);
//		System.out.println(ast.toStringTree());
//		String result = ZimplSTGenerator.generate(ast);
//		System.out.println("zpl: " + result);
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("subto : 0 == 0;\n", result);
	}
	
	@Test void objSumProduct() {
		// objective : ('minimize' | 'maximize') ID? ':' expression;
		String text = "maximize obj: sum <j> in J: 2*x[j] ;";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("maximize obj : sum <j> in J: 2*x[j];\n", result);
	}

	@Test void objSumInt() {
		String text = "maximize : sum <i> in I: 1 ;";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("maximize : sum <i> in I: 1;\n", result);
	}
	
	@Test void objProductIndexed() {
		String result = ZimplSTGenerator.fromString("maximize : 2*x[i];");
		assertEquals("maximize : 2*x[i];\n", result);
	}

	@Test void objProductSimple() {
		String result = ZimplSTGenerator.fromString("maximize : 2*x;");
		assertEquals("maximize : 2*x;\n", result);
	}

	@Test void objParentesis() {
		String result = ZimplSTGenerator.fromString("minimize : (2);");
		assertEquals("minimize : (2);\n", result);
	}
	
	@Test void objMinimalNamed() {
		// objective : ('minimize' | 'maximize') ID? ':' expression;
		String result = ZimplSTGenerator.fromString("maximize obj : 1;");
		assertEquals("maximize obj : 1;\n", result);
	}
	
	@Test void objMinimalUnnamed() {
		String result = ZimplSTGenerator.fromString("minimize : 0;");
		assertEquals("minimize : 0;\n", result);
	}
	
	@Test void varDouble() {
		String text = "var x; var y;";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("var x;\n" + "var y;\n", result);
	}
	
	@Test void varIndexedTypedBoundSingle() {
		String text = "var x[I] real == 0;";
		String result = ZimplSTGenerator.fromString(text);
//		System.out.println("zpl: " + result);
		assertEquals("var x[I] real == 0;\n", result);
	}
	
	@Test void varIndexedTypedUnbound() {
		String result = ZimplSTGenerator.fromString("var x[I] integer;");
		assertEquals("var x[I] integer;\n", result);
	}
	
	// TODO: add tests for varIndexedUntypedBoundDouble() and (signed) infinities 

	@Test void varIndexedUntypedBoundSingle() {
		String result = ZimplSTGenerator.fromString("var x[I] <= 1;");
//		System.out.println("zpl: " + result);
		assertEquals("var x[I] <= 1;\n", result);
	}

	@Test void varIndexedUntypedUnbound() {
		String result = ZimplSTGenerator.fromString("var x[I];");
		assertEquals("var x[I];\n", result);
	}
	
	@Test void varSingleTypedUnbound() {
		String result = ZimplSTGenerator.fromString("var x binary;");
		assertEquals("var x binary;\n", result);
	}
	
	@Test void varSingleUntypedBoundSingle() {
		String result = ZimplSTGenerator.fromString("var x >= 0 ;");
		assertEquals("var x >= 0;\n", result);
	}
	
	@Test void varSingleUntypedUnbound() {
		String result = ZimplSTGenerator.fromString("var x ;");
		assertEquals("var x;\n", result);
	}
	
	@Test void paramDouble() {
		String text = "param P:=0; param Q:=1;";
		String result = ZimplSTGenerator.fromString(text);
		String expected = "param P := 0;\n" + "param Q := 1;\n" ;
		assertEquals(expected, result);
	}

	@Test void paramIndexedDefinedSize2() {
		String text = "param Q[K] := <1> 2, <3> 4 ;";
		AST ast = AstBuilder.fromString(text);
//		System.out.println(ast.toStringTree());
		String result = ZimplSTGenerator.generate(ast);
		assertEquals("param Q[K] := <1> 2, <3> 4;\n", result);
	}
	
	@Test void paramIndexedDefinedSize1() {
		String text = "param Q[J] := <1> 2 ;";
		AST ast = AstBuilder.fromString(text);
		String result = ZimplSTGenerator.generate(ast);
		assertEquals("param Q[J] := <1> 2;\n", result);
	}
	
	// FIXME: Zimpl doesn't seem to restrict zero sized Mappings
//	@Test void paramIndexedDefinedSize0() {
//		String text = "param Q[I] := ;";
//		AST ast = AstBuilder.fromString(text);
//		String result = ZimplSTGenerator.generate(ast);
//		assertEquals("param Q[I] := ;\n", result); 
//	}

	@Test void paramIndexedUndefined() {
		String text = "param Q[U] ;";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("param Q[U];\n", result);
	}
	
	@Test void paramUnindexedDefined() {
		String text = "param D := 1 ;";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("param D := 1;\n", result);
	}
	
	@Test void paramUnindexedUndefined() {
		String text = "param U ;";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("param U;\n", result);
	}

	@Test void setRangeMix() {
		String text = "set RM := {0..N};\n";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals(text, result);
	}
	
	@Test void setRangeId() {
		AST ast = AstBuilder.fromString("set RD := { I..J } ;");
		String result = ZimplSTGenerator.generate(ast);
		assertEquals("set RD := {I..J};\n", result);
	}
	
	@Test void setRangeInt() {
		String text = "set RI := { 0..1 } ;";
		String result = ZimplSTGenerator.fromString(text);
		assertEquals("set RI := {0..1};\n", result);
	}
	
	@Test void setEmpty() {
		AST ast = AstBuilder.fromString("set E := { } ;");
		String result = ZimplSTGenerator.generate(ast);
		assertEquals("set E := { };\n", result);
	}
	
}
