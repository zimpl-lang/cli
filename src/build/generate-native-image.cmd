@echo off
REM run this script in root folder of project 'main';
REM with a repo clone, type: src/build/generate-native-image.cmd
set CORE_CLASSPATH=lib/picocli-4.7.6.jar;lib/antlr4-runtime-4.13.1.jar;lib/zimpl-parser-0.5-SNAPSHOT.jar
set CBC_SOL_CLASSPATH=lib/cbc-sol-adapter-0.1-SNAPSHOT.jar;lib/cbc-sol-parser-0.1-SNAPSHOT.jar
set CSV_CLASSPATH=lib/csv-adapter-0.2-SNAPSHOT.jar;lib/csv-parser-0.1.jar
set DOT_CLASSPATH=lib/dot-adapter-0.2-SNAPSHOT.jar;lib/dot-parser-0.1.jar
set XLSX_CLASSPATH=lib/aalto-xml-1.3.3;lib/commons-compress-1.26.2.jar;lib/commons-io-2.16.1.jar;lib/fastexcel-0.18.1.jar;lib/fastexcel-reader-0.18.1.jar;lib/opczip-1.2.0.jar;lib/stax2-api-4.2.2.jar;lib/xlsx-adapter-0.2-SNAPSHOT.jar
set CLASSPATH=%CORE_CLASSPATH%;%CBC_SOL_CLASSPATH%;%CSV_CLASSPATH%;%DOT_CLASSPATH%;%XLSX_CLASSPATH%
rem set ARTIFACT=zimpler-0.5-SNAPSHOT-YYYYMMDD-HHmm
set ARTIFACT=zimpler-0.5-SNAPSHOT-20240926-1621
rem set ARTIFACT=zimpler-0.5-SNAPSHOT-20240909-1312
rem set ARTIFACT=zimpler

del /Q %ARTIFACT%.exe

REM from https://www.graalvm.org/22.0/reference-manual/native-image/#prerequisites
@echo on
REM On Windows, the native-image builder will only work when it is executed from the x64 Native Tools Command Prompt.
native-image -jar %ARTIFACT%.jar --class-path %CLASSPATH%
