#!/bin/sh
# run this script in root folder of project 'main', type: src/build/generate-reflection-config.sh
export CONFIGPATH="META-INF/native-image"
export PICOCLI_GENPATH="$CONFIGPATH/picocli-generated"
export PROCESSORPATH="lib/picocli-codegen-4.7.6.jar:lib/picocli-4.7.6.jar"
export CORE_CLASSPATH="lib/picocli-4.7.6.jar:lib/antlr4-runtime-4.13.1.jar:lib/zimpl-parser-0.5-SNAPSHOT.jar"
export CBC_SOL_CLASSPATH="lib/cbc-sol-adapter-0.1-SNAPSHOT.jar:lib/cbc-sol-parser-0.1-SNAPSHOT.jar"
export CSV_CLASSPATH="lib/csv-adapter-0.2-SNAPSHOT.jar:lib/csv-parser-0.1.jar"
export DOT_CLASSPATH="lib/dot-adapter-0.2-SNAPSHOT.jar:lib/dot-parser-0.1.jar"
export XLSX_CLASSPATH="lib/aalto-xml-1.3.3.jar:lib/commons-compress-1.26.2.jar:lib/commons-io-2.16.1.jar:lib/fastexcel-0.18.1.jar:lib/fastexcel-reader-0.18.1.jar:lib/opczip-1.2.0:lib/stax2-api-4.2.2.jar:lib/xlsx-adapter-0.2-SNAPSHOT.jar"
export CLASSPATH="${CORE_CLASSPATH}:${CBC_SOL_CLASSPATH}:${CSV_CLASSPATH}:${DOT_CLASSPATH}:${XLSX_CLASSPATH}"
export SRCPATH="src/main/java"

rm -rf "$PICOCLI_GENPATH"

# for native-image reflection config to be generated in $CONFIGPATH, argument '-d .' has to be specified
#javac -Aproject=zimpler -proc:only -d . --processor-path $PROCESSORPATH --class-path $CLASSPATH --source-path $SRCPATH -verbose $SRCPATH/Main.java
javac -Aproject=zimpler -proc:only -d . --processor-path $PROCESSORPATH --class-path $CLASSPATH --source-path $SRCPATH $SRCPATH/Main.java

