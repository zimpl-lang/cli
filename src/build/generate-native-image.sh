#!/bin/zsh
# in root folder of project 'main', run src/build/generate-native-image.sh
export CORE_CLASSPATH="lib/picocli-4.7.6.jar:lib/antlr4-runtime-4.13.1.jar:lib/zimpl-parser-0.5-SNAPSHOT.jar"
export CBC_SOL_CLASSPATH="lib/cbc-sol-adapter-0.1-SNAPSHOT.jar:lib/cbc-sol-parser-0.1-SNAPSHOT.jar"
export CSV_CLASSPATH="lib/csv-adapter-0.2-SNAPSHOT.jar:lib/csv-parser-0.1.jar"
export DOT_CLASSPATH="lib/dot-adapter-0.2-SNAPSHOT.jar:lib/dot-parser-0.1.jar"
export XLSX_CLASSPATH="lib/aalto-xml-1.3.3.jar:lib/commons-compress-1.26.2.jar:lib/commons-io-2.16.1.jar:lib/fastexcel-0.18.1.jar:lib/fastexcel-reader-0.18.1.jar:lib/opczip-1.2.0:lib/stax2-api-4.2.2.jar:lib/xlsx-adapter-0.2-SNAPSHOT.jar"
export CLASSPATH="${CORE_CLASSPATH}:${CBC_SOL_CLASSPATH}:${CSV_CLASSPATH}:${DOT_CLASSPATH}:${XLSX_CLASSPATH}"
#export MAIN_REPO="https://gitlab.com/zimpler-tool/main/-/raw/main/"
export MAIN_REPO="https://gitlab.com/zimpler-tool/main/raw/main/"
export NATIVEIMAGE_PATH="META-INF/native-image/"
export NATIVEIMAGE_REPO="${MAIN_REPO}${NATIVEIMAGE_PATH}"
#export ARTIFACT="zimpler-0.5-SNAPSHOT-YYYYMMDD-HHmm"
ARTIFACT="zimpler-0.5-SNAPSHOT-20240926-1621"
#export ARTIFACT="zimpler-0.5-SNAPSHOT-20240909-1312"
#export ARTIFACT="zimpler-0.5-SNAPSHOT-20240814-1836"
#export ARTIFACT="zimpler-0.5"

rm -f "$ARTIFACT"

if [ ! -f "provision-artifacts.sh" ]; then
    echo "Provisioning artifacts..."
    wget -q "${MAIN_REPO}src/build/provision-artifacts.sh"
    chmod +x "provision-artifacts.sh"
fi

./provision-artifacts.sh

reflect_configs=("main" "zimpl-parser")
for cfg in ${reflect_configs} ; do
    if [ ! -d "${NATIVEIMAGE_PATH}${cfg}" ]; then
    	echo "creating directory ${NATIVEIMAGE_PATH}${cfg}"
        mkdir -p "${NATIVEIMAGE_PATH}${cfg}"
        echo "downloading reflection config '${cfg}' from ${NATIVEIMAGE_REPO}${cfg}"
        wget -qO "${NATIVEIMAGE_PATH}${cfg}/reflect-config.json" "${NATIVEIMAGE_REPO}${cfg}/reflect-config.json"
    fi
done

if [ ! -f "${NATIVEIMAGE_PATH}proxy-config.json" ]; then
    echo "downloading proxy config from ${NATIVEIMAGE_REPO}proxy-config.json"
    wget -qO "${NATIVEIMAGE_PATH}proxy-config.json" "${NATIVEIMAGE_REPO}proxy-config.json"
fi

# check if program is installed
if ! command -v native-image &> /dev/null
then
    echo "Command 'native-image' (from project GraalVM) could not be found"
    echo "It can be installed from: https://www.graalvm.org/downloads/"
    exit 1
fi

native-image -jar "${ARTIFACT}".jar --class-path ${CLASSPATH}

