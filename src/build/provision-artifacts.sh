#!/bin/zsh
# in root folder of project 'main', run src/build/provision-dependencies.sh
BIN_REPO="https://gitlab.com/zimpler-tool/bin/raw/main/"
BUILD_REPO="https://gitlab.com/zimpler-tool/main/raw/main/src/build/"
MAVEN_REPO="https://repo1.maven.org/maven2/"
LIB_PATH="lib/"
#ARTIFACT_JAR="zimpler-0.5-SNAPSHOT-YYYYMMDD-HHmm.jar"
ARTIFACT_JAR="zimpler-0.5-SNAPSHOT-20240926-1621.jar"
#ARTIFACT_JAR="zimpler-0.5-SNAPSHOT-20240909-1312.jar"
#ARTIFACT_JAR="zimpler-0.5-SNAPSHOT-20240814-1836.jar"

# Define an associative array
declare -A map
map[aalto-xml-1.3.3.jar]="${MAVEN_REPO}com/fasterxml/aalto-xml/1.3.3/"
map[antlr4-runtime-4.13.1.jar]="${MAVEN_REPO}org/antlr/antlr4-runtime/4.13.1/"
map[cbc-sol-adapter-0.1-SNAPSHOT.jar]=${BIN_REPO}
map[cbc-sol-parser-0.1-SNAPSHOT.jar]=${BIN_REPO}
map[commons-compress-1.26.2.jar]="${MAVEN_REPO}org/apache/commons/commons-compress/1.26.2/"
map[commons-io-2.16.1.jar]="${MAVEN_REPO}commons-io/commons-io/2.16.1/"
map[csv-adapter-0.2-SNAPSHOT.jar]=${BIN_REPO}
map[csv-parser-0.1.jar]=${BIN_REPO}
map[dot-adapter-0.2-SNAPSHOT.jar]=${BIN_REPO}
map[dot-parser-0.1.jar]=${BIN_REPO}
map[fastexcel-0.18.1.jar]="${MAVEN_REPO}org/dhatim/fastexcel/0.18.1/"
map[fastexcel-reader-0.18.1.jar]="${MAVEN_REPO}org/dhatim/fastexcel-reader/0.18.1/"
map[opczip-1.2.0.jar]="${MAVEN_REPO}com/github/rzymek/opczip/1.2.0/"
map[picocli-4.7.6.jar]="${MAVEN_REPO}info/picocli/picocli/4.7.6/"
map[stax2-api-4.2.2.jar]="${MAVEN_REPO}org/codehaus/woodstox/stax2-api/4.2.2/"
map[xlsx-adapter-0.2-SNAPSHOT.jar]=${BIN_REPO}
map[zimpl-parser-0.5-SNAPSHOT.jar]=${BIN_REPO}
map[${ARTIFACT_JAR}]=${BIN_REPO}

# OSTYPEs: "linux-gnu", *linux*, "darwin", darwin*, "win32"?
if [[ "$OSTYPE" == "win"* ]]; then
	EXTENSION="cmd"
else
	EXTENSION="sh"
fi
map[generate-native-image.${EXTENSION}]=${BUILD_REPO}

if [ ! -d $LIB_PATH ]; then
	echo "Creating directory $LIB_PATH"
	mkdir $LIB_PATH
fi
for filename url in ${(kv)map} ; do
	if [ ! -f $LIB_PATH$filename ]; then
		echo "downloading ${filename} from ${map[$filename]}"
		wget -qO "$LIB_PATH$filename" "$url$filename"
	fi
done
if [ ! -f $ARTIFACT_JAR ]; then
	if [ -f $LIB_PATH$ARTIFACT_JAR ]; then
		mv $LIB_PATH$ARTIFACT_JAR "."
	fi
fi
if [ ! -f "generate-native-image."${EXTENSION} ]; then
	mv "${LIB_PATH}generate-native-image.${EXTENSION}" "."
	chmod +x "generate-native-image.${EXTENSION}"
fi

