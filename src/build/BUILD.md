# Building instructions

## Requisites
- GraalVM: to use the `native-image` tool.
- In Mac, the clang compiler is called from `native-image`.
- In Linux, the GCC compiler is called from `native-image`.
- In Windows, the MSVC compiler is called from `native-image`.
- File `picocli-codegen-4.7.*.jar` in lib folder: to generate `native-image` config.

## Steps
1. Review `META-INF/MANIFEST.MF`
	1. check `Main-Class`
	2. update `Class-Path` if necessary
2. Generate jar file
3. Review PATHs in scripts from src/build
4. If SNAPSHOT version
	1. timestamp jar
	2. upload jar to `bin` repo
	3. update artifact timestamp in `provision-artifacts.sh` and `generate-native-image.*`
	4. commit changes
5. Generate native image
6. Update [Download links](https://gitlab.com/zimpler-tool/gitlab-profile/blob/main/README.md#download)


## Generate jar file `zimpler.jar`

This instructions assume you're using the Eclipse IDE. Adapt them for other IDEs and please contribute that back.

### Automatic generation with the Eclipse IDE
 - Open `build.jardesc` and click "Finish".

### Manual generation with the Eclipse IDE
- Select the project in the Package Explorer.
- In the menu bar, choose File > Export > Java > JAR file.
- Select folder `src/main/java` and files:
	- `META-INF/MANIFEST.MF`
	- `LICENSE`
	- `README.md`
- Deselect everything else.
- Fill in "JAR file" with "main/zimpler-${version}.jar".
- Click "Finish".


## Generate Native Image
Platform and context specific instructions allow to generate a native executable image. This image will be built from the Java compiled code in a Jar file, statically linked to its dependencies and parts of the Java Runtime, resulting in a ready-to-use standalone program.

### Manual Creation in Linux and Mac (from repo clone)
- With a terminal at the project's root folder, run `src/build/generate-native-image.sh`

### Manual Creation in Linux and Mac (from scratch)
1. Download [this shell script](https://gitlab.com/zimpler-tool/main/raw/main/src/build/generate-native-image.sh)
2. Grant it execution permission (`chmod +x generate-native-image.sh`)
3. Run it!

### Manual Creation in Windows (from scratch)
In addition to the [Requisites](BUILD.md#requisites), [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/about) helps the following to work smoothly.
<!-- 1. Download [`provision-artifacts.sh`](https://gitlab.com/zimpler-tool/main/raw/main/src/build/provision-artifacts.sh) and [`generate-native-image.cmd`](https://gitlab.com/zimpler-tool/main/raw/main/src/build/generate-native-image.cmd). -->
1. Download [`provision-artifacts.sh`](https://gitlab.com/zimpler-tool/main/raw/main/src/build/provision-artifacts.sh).
2. Run `bash` to start a linux session.
3. If `zsh` is not installed, run `sudo apt install zsh`.
4. Run `./provision-artifacts.sh` to download required artifacts.
5. Type `exit` to finish the linux session.
6. Run `generate-native-image.cmd`.
7. Enjoy!


**TODO**: document generation of reflection config
<!-- Try to convince Eclipse to run picocli-codegen annotation processor -->

## Create zimpler-${version}-sources.zip
**TODO**

## Create zimpler-${version}-javadoc.zip
**TODO**

**TODO**: autocomplete ${version}
