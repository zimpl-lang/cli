@echo off
set WORKDIR=%1

REM convert model from .zpl to .lp format; adds '.lp' suffix
call zimpl -l 100 -o "%WORKDIR%\model" "%WORKDIR%\model.zimpl"

REM solve lp with COIN-OR CBC solver - https://github.com/coin-or/Cbc
REM see http://www.decom.ufop.br/haroldo/files/cbcCommandLine.pdf
REM https://raw.githubusercontent.com/coin-or/COIN-OR-OptimizationSuite/master/Installer/files/doc/cbcCommandLine.pdf
call cbc "%WORKDIR%\model.lp" solve solution "%WORKDIR%\model.sol"

REM Zimpler.CliOptimizer expects solution to be saved in %WORKDIR% as "model.sol"

