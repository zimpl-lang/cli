#!/bin/zsh

declare -A version
version[zimpl]="3.6.2"
version[cbc]="2.10.12"

workdir="$1"

# echo "workdir: $workdir"

# Function to prompt for yes/no input
prompt_yes_no() {
    while true; do
        echo "$1 (y/n): "	# print prompt
        read user_choice	# read input
        case "$user_choice" in
            [Yy]* ) return 0;;  # User answered yes
            [Nn]* ) return 1;;  # User answered no
            * ) echo "Please answer yes (y) or no (n).";;  # Invalid input
        esac
    done
}

locate() {
	if command -v "./$1" &> /dev/null; then
		# command found in current directory "./"
		echo "./$1"
	elif command -v "$1" &> /dev/null; then
		# command found in the system path
		echo "$1"
	else
		# command NOT found
		echo ""
	fi
}

download() {
	echo "'$1' not found."
	# OSTYPEs: "linux-gnu", *linux*, "darwin", darwin*, "win32"?
	if [[ "$OSTYPE" == "darwin"* ]]; then
		local QUALIFIER="mac-intel"
	elif [[ "$OSTYPE" == "linux"* ]]; then
		local QUALIFIER="linux"
	else
		echo "Unrecognized system. Can't download $1 automatically."
		show_download_options "$1"
		exit 1
	fi

	# Check if confirmation has already been given
	if [[ -z $AGREED ]]; then
	    prompt_yes_no "Do you want to download missing programs?"
	    AGREED=$? # Capture exit status
	fi

	if (( $AGREED == 0 )) ; then
	    local BIN_REPO="https://gitlab.com/zimpler-tool/bin/raw/main/"
	    local filename="$1-${version[$1]}"
	    local url="${BIN_REPO}${filename}-${QUALIFIER}"
	    echo "downloading '$1' from $url"
	    curl -so "$1" "$url"
	    chmod +x "$1"
	else
	    echo "You chose not to download '$1'."
	    show_download_options "$1"
	    exit 1
	fi
}

show_download_options() {
	echo "Download failed. You may be able to get '$1' from:"
	if [[ "$1" == "zimpl" ]]; then
		echo " - https://anaconda.org/conda-forge/zimpl/files (newer versions)"
		echo " - https://zimpl.zib.de/download/ (older versions)"
	elif [[ "$1" == "cbc" ]]; then
		echo " - https://github.com/coin-or/Cbc/ (newer versions)"
		echo " - https://www.coin-or.org/download/binary/Cbc/ (older versions)"
	else
		echo "ERROR: unexpected program '$1'"
	fi
}

provision() {
	CMD=$(locate "$1")
	#echo "Located $1 as $CMD"
	if [[ "$CMD" == "" ]]; then
		download "$1"
		CMD=$(locate "$1")
		#echo "relocated $1: $CMD"
		if [[ "$CMD" == "" ]]; then
			#echo "Failed download"
			show_download_options "$1"
			exit 1
		fi
	fi
	echo "$CMD"
}

ZIMPL=$(provision "zimpl")
# convert model.zimpl to model.lp; zimpl adds '.lp' suffix
${ZIMPL} -l100 -v0 -o "${workdir}/model" "${workdir}/model.zimpl"

if [ ! -f "${workdir}/model.lp" ]; then
	echo "Error generating ${workdir}/model.lp"
	exit 1
fi

CBC=$(provision "cbc")
#echo "Provisioned 'cbc' as $CBC"

# solve lp with COIN-OR CBC solver - https://github.com/coin-or/Cbc
# see http://www.decom.ufop.br/haroldo/files/cbcCommandLine.pdf
# https://raw.githubusercontent.com/coin-or/COIN-OR-OptimizationSuite/master/Installer/files/doc/cbcCommandLine.pdf
${CBC} "${workdir}/model.lp" solve solution "${workdir}/model.sol" > /dev/null

if [ ! -f "${workdir}/model.sol" ]; then
	echo "Error generating ${workdir}/model.sol"
	exit 1
else
	echo "solution found"
fi
# Zimpler.CliOptimizer expects solution to be saved in $workdir as "model.sol"
