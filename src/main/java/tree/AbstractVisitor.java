package tree;

import java.io.IOException;

import de.zib.zimpl.antlr.FormulationParser;

/**
 * @inheritDoc
 * 
 * @author Javier MV
 * @since 0.2
 */
public abstract class AbstractVisitor<T> implements Visitor<T> {

	protected static <T> T
	visitFromFilename(String filename, Visitor<T> visitor)
	throws IOException {
		AST tree = AstBuilder.fromFilename(filename);
		return tree.accept(visitor);
	}

	protected static <T> T
	visitFromString(String text, Visitor<T> visitor) {
		return visitFromString(text, visitor, 
				FormulationParser.RULE_formulation);
	}
	
	protected static <T> T
	visitFromString(String text, Visitor<T> visitor, int parseRule) {
		AST tree = AstBuilder.fromString(text, parseRule);
		return tree.accept(visitor);
	}
	
	public T visit(AST node) {
		if (node == null)
			return null;
		T result = visitToken(node);
		if (node.getChildCount() == 0)
			return result;
		for (int i = 0; i < node.children.size(); ++i) {
			AST child = node.getChild(i);
			if (child == null)
				continue;
			T childResult = child.accept(this);
			result = reduce(result, childResult, i);
		}
		return result;
	}
	
	public T visitToken(AST node)			{ return null; }
	public T visitCommand(Command node)		{ return visit(node); }
	public T visitCheck(Check node)			{ return visit(node); }
	public T visitPrint(Print node)			{ return visit(node); }
	public T visitSetDecl(SetDecl node)		{ return visit(node); }
	public T visitSetDesc(SetDesc node)		{ return visit(node); }
	public T visitSetEmpty(SetEmpty node)	{ return visit(node); }
	public T visitParam(Param node) 		{ return visit(node); }
	public T visitMapping(Mapping node)		{ return visit(node); }
	public T visitChain(Chain node)			{ return visit(node); }
	public T visitDuo(Duo node)				{ return visit(node); }
	public T visitReadfile(Readfile node)	{ return visit(node); }
	public T visitVarDecl(VarDecl node) 	{ return visit(node); }
	public T visitVarRef(VarRef node) 		{ return visit(node); }
	public T visitObjective(Objective node)	{ return visit(node); }
	public T visitConstraint(Constraint node){return visit(node); }
	public T visitForall(Forall node)		{ return visit(node); }
	public T visitFnDecl(FnDecl node) 		{ return visit(node); }
	public T visitFnRef(FnRef node) 		{ return visit(node); }
	public T visitOp(OpNode node)			{ return visit(node); }
	public T visitParen(Paren node)			{ return visit(node); }
	public T visitRange(Range node) 		{ return visit(node); }
	public T visitTuple(Tuple node)			{ return visit(node); }
	public T visitSum(Sum node) 			{ return visit(node); }
	public T visitCondition(Condition node)	{ return visit(node); }
	public T visitComparison(Comparison node){return visit(node); }
	public T visitBound(Bound node)			{ return visit(node); }
	public T visitInfinity(Infinity node)	{ return visit(node); }
	public T visitCsv(CSV node)				{ return visit(node); }
	public T visitIf(If node)				{ return visit(node); }
	
	/**
	 * Combines the current and next results.
	 * 
	 * @param current Visitor's partial result; it's associated with the
	 * 	token and node being visited, including up to its i-1 child.
	 * @param next Result of the 'i'th child accepting this Visitor.
	 * @param i Index of the child associated with the next result.
	 * @return Combination of input parameters.
	 */
	public abstract T reduce(T current, T next, int i);
	
	protected T reduceNext(T current, T next, int i) { return next; }
	
	protected AST reduceRewriteAst(AST current, AST next, int i) {
		assert current != null;
		current.children.set(i, next);
		return current; 
	}
	
	/** Forwards traversal by calling <code>accept(this)</code> on
	 * the node argument.
	 */
	public T fwd(AST node) { return node.accept(this); }
	
}
