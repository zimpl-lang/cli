package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>forall</b>: 'forall' condition ('do' | ':')
 * 		child=( forall | comparison ) ;
 * // TODO: update javadoc
 * 
 * @author Javier MV
 * @since 0.3
 */
public class Forall extends AST {
	
	public static final Token token = 
			AST.getToken(zimpl.ZimplParser.RULE_forall, "forall");
	
	// it's an ugly hack, having to define a constructor with no nested
	// forall child, if antlr4 really can't produce λ (the empty string)
	// from tail recursion. it forces a setNested method and additional
	// setup in the context where it is used (constraint and do-command).
	public Forall(Condition condition, Token separator) {
		this(condition, separator, null);
	}
	public Forall(Condition condition, Token separator, Forall nested) {
		super(token, Arrays.asList(condition, new AST(separator), nested));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitForall(this);
	}
	
	public Condition getCondition() { return (Condition) getChild(0); }
	// @see {Sum,Red,Condition}.getSeparator()
	public String getSeparator() { return getChild(1).token.getText(); }
	
	public Forall getNested() { return (Forall) getChild(2); }
	
	public Forall setNested(Forall nested) {
		children.set(2, nested);
		return this;
	}
	
}
