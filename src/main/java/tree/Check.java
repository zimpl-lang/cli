package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * TODO: write javadoc
 * 
 * @author Javier MV
 * @since 0.5
 */
public class Check extends AST {
	
	public static final Token token = 
			AST.getToken(zimpl.ZimplParser.RULE_check, "check");
	
	// TODO: if Condition is renamed as Member, boolExpr here could be renamed to 'condition'
	public Check(AST boolExpr) { super(token, Arrays.asList(boolExpr)); }
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitCheck(this);
	}
	
	// TODO: if Condition is renamed as Member, boolExpr here could be renamed to 'condition'
	public AST getBoolExpr() { return children.get(0); }
	
}
