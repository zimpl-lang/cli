package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * TODO: write javadoc
 * 
 * @author Javier MV
 * @since 0.5
 */
public class Command extends AST {
	
	public static final Token token = 
			AST.getToken(zimpl.ZimplParser.RULE_command, "do");
	
	public Command(Forall forall, AST cmd) {
		super(token, Arrays.asList(forall, cmd));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitCommand(this);
	}
	
	public Forall getForall() { return (Forall) children.get(0); }
	
	// TODO: rename this
	public AST getCmd() { return children.get(1); }
	
}
