package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * @author Javier MV
 * @since 0.3
 */
public class Readfile extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_readfile, "read");
	
	/**
	 * <b>readfile</b>: 'read' filename=STRING 'as' template=STRING
	 * 		('skip' skip=INT)? ('use' use=INT)? ('match' match=STRING)?
	 * 		('comment' comment=STRING)? ;
	 */
	public Readfile(AST filename, AST template, AST skip, AST use,
			AST match, AST comment) {
		super(token, Arrays.asList(filename, template, skip, use,
				match, comment));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitReadfile(this);
	}
	
	public String getFilename()	{ return getChild(0).token.getText(); }
	
	public String getTemplate()	{ return getChild(1).token.getText(); }
	
	public AST getSkip() 	{ return getChild(2); }
	
	public AST getUse() 	{ return getChild(3); }
	
	public AST getMatch() 	{ return getChild(4); }
	
	public AST getComment()	{ return getChild(5); }
	
}
