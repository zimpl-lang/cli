package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * mapping : ( readfile | chain | readfile ',' chain) ('default' elem)? ;

 * @author Javier MV
 * @since 0.3
 */
public class Mapping extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_mapping, "⇶");
	
	public Mapping(Readfile readfile, Chain chain, AST defaultValue) { 
		super(token, Arrays.asList(readfile, chain, defaultValue));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitMapping(this);
	}
	
	public Readfile getReadfile() { return (Readfile) getChild(0); }
	
	public Chain getChain() { return (Chain) getChild(1); }
	
	public AST getDefault() { return getChild(2); }
	
}
