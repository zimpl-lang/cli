package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * @author Javier MV
 * @since 0.5
 */
public class If extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_ifExpr, "if");
	
	public If(AST boolExpr, AST thenExpr, AST elseExpr) {
		super(token, Arrays.asList(boolExpr, thenExpr, elseExpr));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitIf(this);
	}
	
	public AST getBoolExpr() { return getChild(0); }
	public AST getThenExpr() { return getChild(1); }
	public AST getElseExpr() { return getChild(2); }
	
}
