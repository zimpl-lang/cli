package tree;

import java.util.List;

import org.antlr.v4.runtime.Token;

/**
 * Represents Comma Separated lists of Values, either in their grammar
 * rule form or otherwise. Known uses: <ul>
 * 	<li><b>csv</b>		: elem (',' elem)* ; </li>
 * 	<li><b>idList</b>	: (ID (',' ID)*)? ; </li>
 * 	<li><b>setDesc</b>	: '{' tuple (',' tuple)* '}'	| ... </li>
 * </ul>
 * @author Javier MV
 * @since 0.4
 */
public class CSV extends AST {
	// NOTE: if this class is going to represent values from several
	// rules, maybe defining type as RULE_csv seems confusing. See
	// Expression.g4:csv; maybe all the current uses can be replaced by
	// a single 'csv', delegating datatype enforcement out of the parser
	// and, therefore simplifying language rules. For this last reason,
	// for the time being, RULE_csv remains as token's type.
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_csv, ",");
	
	public CSV(List<AST> elems) { super(token, elems); }
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitCsv(this);
	}
	
	public List<AST> getElems() { return children; }
	
}
