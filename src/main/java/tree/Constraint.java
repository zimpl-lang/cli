package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>constraint</b>: 'subto' name=ID? ':' forall ;
 * 
 * //FIXME: update javadoc
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Constraint extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_constraint, "subto");
	
	public Constraint(AST name, Forall forall, Comparison comparison) {
		super(token, Arrays.asList(name, forall, comparison));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitConstraint(this);
	}
	
	public String getName() {
		return (getChild(0) == null) ? null :
				getChild(0).token.getText();
	}
	
	public Forall getForall() { return (Forall) getChild(1); }
	public Comparison getComparison() { return (Comparison) getChild(2); }
	
}
