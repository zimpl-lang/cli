package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>objective</b>: GOAL name=ID? ':' expr ;
 * <b>GOAL</b>: 'min' | 'minimize' | 'max' | 'maximize' ;
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Objective extends AST {
	
	public Objective(Token token, AST name, AST expression) {
		super(token, Arrays.asList(name, expression));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitObjective(this);
	}
	
	public String getName() { // TODO: should return AST?
		// return getChild(0); ? // returning String is easier on
		// StringTemplate; returning AST seems easier on Visitors :o/
		// see ZimplVisitor.visitVar().type and .visitObj().name.
		return (getChild(0) == null)? null :
				getChild(0).token.getText();
	}
	
	public AST getExpression() { return getChild(1); }
	
}
