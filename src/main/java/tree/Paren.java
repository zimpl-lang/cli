package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * This class represents an expression in parentheses. It comes from: <br/>
 * basicExpr: <b>'('</b> expr <b>')'</b> | ... ;
 * 
 * @author Javier MV
 * @since 0.3
 */
public class Paren extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.PAREN, "()");
	
	public Paren(AST expr) {
		super(token, Arrays.asList(expr));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitParen(this);
	}
	
	public AST getExpr() { return getChild(0); }
	
}
