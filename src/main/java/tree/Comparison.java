package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>comparison</b>: nExpr bound+ ; <br/>
 * <b>bound</b> : cmp ( nExpr | sign=('+'|'-')? 'infinity' ) ;
 * 
 * // TODO: update javadoc
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Comparison extends AST {
	
	public Comparison(AST lhs, Token op, AST rhs) {
		super(op, Arrays.asList(lhs, rhs));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitComparison(this);
	}
	
	public AST getLhs() { return getChild(0); }
	public AST getRhs() { return getChild(1); }
	
}
