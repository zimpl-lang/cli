package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>setDesc</b> :	'{' (setEmpty) | csv | range | condition |
 * 		tuple (',' tuple)* '}' <p/>
 * <b>NOTE:</b> comma separated lists of tuples are treated as csv.
 * 
 * @author Javier MV
 * @since 0.3
 */
public class SetDesc extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_setDesc, "{}");
	
	public SetDesc(AST child) { super(token, Arrays.asList(child)); }
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitSetDesc(this);
	}
	
	public AST getChild() { return getChild(0); }
	
}
