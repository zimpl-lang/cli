package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * TODO: write javadoc
 * 
 * @author Javier MV
 * @since 0.5
 */
public class Print extends AST {
	
	public static final Token token = 
			AST.getToken(zimpl.ZimplParser.RULE_print, "print");
	
	public Print(AST values) { super(token, Arrays.asList(values)); }
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitPrint(this);
	}
	
	public AST getValues() { return children.get(0); }
	
}
