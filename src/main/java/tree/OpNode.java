package tree;

import java.util.List;

import org.antlr.v4.runtime.Token;

/**
 * This class represents the application of an operand in { +, - *, / }
 * to a non-empty sequences of uExpr.
 * Currently, only 2 situations are considered: <br/>
 * 	&emsp; uExpr: uExpr <b>op=('*'|'/'|'+'|'-')</b> uExpr | ... ; <br/>
 * as a binary application, and <br/>
 * 	&emsp; expr: sign=('+'|'-')? uExpr ; <br/>
 * as a unary application. The n-ary application is yet to be implemented.
 * <p/>
 * This class used to represent binary arithmetic operations only. They
 * came from: <br/>
 * <ul>
 * <li>Arithmetic u(nsigned)Expr(essions)</li> 
 * uExpr: uExpr <b>op=('*'|'/')</b> uExpr 
 * 		| uExpr <b>op=('+'|'-')</b> uExpr | ... ;
 * 
 * <li>Set Expr(essions)</li>
 * setExpr : setExpr <b>op=('*'|'+'|'\'|'-')</b> setExpr | ... ;
 * </ul>
 * 
 * @author Javier MV
 * @since 0.2
 */
public class OpNode extends AST {

	public OpNode(Token op, List<AST> expressions) {
		super(op, expressions);
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitOp(this);
	}
	
	public List<AST> getValues() { return children; }
	
}
