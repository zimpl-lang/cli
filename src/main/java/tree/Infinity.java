package tree;

import java.util.Arrays;
import java.util.List;

import org.antlr.v4.runtime.Token;

/**
 * @author Javier MV
 * @since 0.3
 */
public class Infinity extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.INFINITY, "∞");
	
	// TODO: see if (unary use of) OpNode isn't a simpler fit for 'sign'
	private static List<AST> optionalList(Token token) {
		return token == null ? null : Arrays.asList(new AST(token));
	}
	
	public Infinity(Token sign) { super(token, optionalList(sign)); }
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitInfinity(this);
	}
	
	public AST getSign() { return optGetChild(0); }
	
}
