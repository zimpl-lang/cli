package tree;

import java.util.List;

import org.antlr.v4.runtime.Token;

/**
 * <b>chain</b>: ( duo (',' duo)* )? ;
 * 
 * @author Javier MV
 * @since 0.3
 */
public class Chain extends AST {
	
	public static final Token token = 
			AST.getToken(zimpl.ZimplParser.RULE_chain, "⇉");
	
	public Chain(List<AST> duos) {
		super(token, duos);
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitChain(this);
	}
	
	public List<AST> getDuos() { return children; }
	
}
