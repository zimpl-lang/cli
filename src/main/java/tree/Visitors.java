package tree;

import java.io.IOException;
import java.util.List;

/**
 * Convenience methods for Visitors.
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Visitors {

	/**
	 * Applies visitors sequentially on AST built from reading file.
	 * 
	 * @param <T> Return type of the last Visitor in the sequence.
	 * @param filename Name of file to read.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T
	reduceFromFilename(String filename, List<Visitor<?>> visitors)
	throws IOException {
		Object result = AstBuilder.fromFilename(filename);
		for (Visitor<?> it : visitors) {
			try {
				result = ((AST) result).accept(it);
			} catch (Exception e) {
				if (e.getCause() instanceof NoSuchMethodException) {
					// This happens if Parsers.applyRule() can't find a
					// method of ZimplParser, after AOT compilation by
					// GraalVM's native-image generator, with missing
					// reflection metadata.
					throw e;
				}
				String msg = "Unexpected Exception in Visitor " + it.getClass().getCanonicalName();
				throw new RuntimeException(msg, e);
			}
		}
		return (T) result;
	}
	
}
