package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>condition</b>: tuple 'in' setExpr (('with' | '|') boolExpr)? ; <br/>
 * 
 * In Section 4.5, [Koch2020] calls this an 'index'.
 * 
 * // TODO: should be renamed 'member(ship)'?
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Condition extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_condition, "in");
	
	public Condition(Tuple tuple, AST setExpr, AST separator, AST predicate) {
		super(token, Arrays.asList(tuple, setExpr, separator, predicate));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitCondition(this);
	}
	
	public Tuple getTuple() 	{ return (Tuple) getChild(0); }
	public AST getSetExpr() 	{ return getChild(1); }
	// @see {Sum,Red,Forall}.getSeparator()
	public String getSeparator(){ return getChild(2).toString(); }
	public AST getPredicate() 	{ return getChild(3); }
	
}
