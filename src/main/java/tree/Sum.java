package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/** 
 * <b>sumExpr</b> : 'sum' condition ('do' | ':') nExpr ;
 * 
 * // TODO: update javadoc
 * // TODO: rename Sum -> Red(uce)
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Sum extends AST {
	
//	public static final Token token =
//			AST.getToken(zimpl.ZimplParser.RULE_sumExpr, "sum");
	
//	public Sum(Condition condition, AST expr) {
	public Sum(Token op, Condition condition, Token separator, AST expr) {
		super(op, Arrays.asList(condition, new AST(separator), expr));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitSum(this);
	}
	
	public Condition getCondition() { return (Condition) getChild(0); }
	
	// @see {Forall,Condition}.getSeparator()
//	public String getSeparator() { return getChild(1).token.getText(); }
	public String getSeparator() { return getChild(1).toString(); }
	
	public AST getExpr() { return getChild(2); }
	
}
