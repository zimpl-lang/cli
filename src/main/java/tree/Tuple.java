package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>tuple</b>: '<' csv '>' ;
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Tuple extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_tuple, "<>");
	
	public Tuple(AST csv) { super(token, Arrays.asList(csv)); }
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitTuple(this);
	}
	
	public AST getCsv() { return getChild(0); }
	
}
