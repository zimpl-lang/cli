package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * This class represents a set of values.
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Range extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_range, "..");
	
	/** <b>range</b> : lhs=expr '..' rhs=expr ; */
	public Range(AST lhs, AST rhs) {
		super(token, Arrays.asList(lhs, rhs));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitRange(this);
	}
	
	public AST getLhs() { return getChild(0); }
	
	public AST getRhs() { return getChild(1); }
	
}
