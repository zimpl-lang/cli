package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>duo</b>: tuple elem ;
 * 
 * @author Javier MV
 * @since 0.3
 */
public class Duo extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_duo, "→");
	
	/** <b>duo</b>: tuple elem ; */
	public Duo(Tuple tuple, AST elem) {
		super(token, Arrays.asList(tuple, elem));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitDuo(this);
	}
	
	public Tuple getTuple() { return (Tuple) getChild(0); }
	
	public AST getElem() { return getChild(1); }
	
}
