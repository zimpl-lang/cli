package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>function:</b> FN_TYPE name=ID '(' csv ')' (':=' expr)? ; <br/>
 * <b>FN_TYPE :</b> 'defnumb' | 'defstrg' | 'defbool' | 'defset' ; <br/>
 * <b>expr:</b> setExpr | expr | boolExpr | STRING ;
 * 
 * @author Javier MV
 * @since 0.3
 */
public class FnDecl extends AST {
	
	public static final Token token = 
			AST.getToken(zimpl.ZimplParser.RULE_function, "𝑓");
	
	public FnDecl(AST type, AST name, AST ids, AST def) {
		super(token, Arrays.asList(type, name, ids, def));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitFnDecl(this);
	}
	
	public String getType() { return getChild(0).toString(); }
	public String getName() { return getChild(1).toString(); }
	public AST getIds() 	{ return getChild(2); }
	public AST getDef() 	{ return getChild(3); }
	
}
