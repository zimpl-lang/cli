package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>variable</b>: 'var' name=ID ('[' index ']')? TYPE? b1=bound? b2=bound? ; <br/>
 * <b>TYPE</b>    : 'real' | 'binary' | 'integer' ; <br/>
 * 
 * @author Javier MV
 * @since 0.2
 */
public class VarDecl extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_variable, "var");
	
	// FIXME: it looks like name & index should share a common IdRef
	// or VarRef AST node, see also Param & SetNode.
	public VarDecl(AST name, AST index, AST type, Bound b1, Bound b2) {
		super(token, Arrays.asList(name, index, type, b1, b2));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitVarDecl(this);
	}
	
	// TODO: there are several uses of getChild(i).token.getText()
	// across AST subclasses; refactor as a shared method.
	public String	getName() 	 { return getChild(0).token.getText(); }
	public AST		getIndex() 	 { return getChild(1); }
	public String 	getType() 	 { // TODO: should return AST?
		// return getChild(2); ? // returning String is easier on 
		// StringTemplate; returning AST seems easier on Visitors :o/
		// see ZimplVisitor.visitVar().type and .visitObj().name.
		return (getChild(2) == null)? null :
			getChild(2).token.getText(); 
	}
	public Bound getBound1() { return (Bound) getChild(3); }
	public Bound getBound2() { return (Bound) getChild(4); }
	
}
