package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * Parameter
 *
 * @author Javier MV
 * @since 0.2
 */
public class Param extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_parameter, "param");
	
	/**
	 * <table>
	 * <tr> <td><b>parameter</b></td> 
	 * 		<td>: 'param' name=ID ( ':=' elem )?</td><td>#ParamSingle</td> </tr>
	 * <tr> <td/> <td>| 'param' name=ID ':=' readfile</td>
	 * 		<td>#ParamReadValue</td></tr>
	 * <tr> <td/> <td>| 'param' name=ID '[' index ']' ( ':=' mapping )?</td>
	 * 		<td>#ParamMany</td></tr>
	 * <tr> <td/> <td>;</td>
	 * </table>
	 * <b>FIXME:</b> update table above with new grammar rule definition.
	 */
	public Param(AST name, AST index, AST value) {
		// TODO: name & index should be replaced by sqRef
		super(token, Arrays.asList(name, index, value));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitParam(this);
	}
	
	public String getName()	{ return getChild(0).token.getText(); }
	
	// FIXME: rename VarRef -> SqRef
	public VarRef getRef()		{ return (VarRef) getChild(0); }
	
	// TODO: method getIndex is ok but 'index' should be child[0].index
	// and child[1] should be removed, replaced by current child[2]
	public AST getIndex()	{ return getChild(1); }
	
	public AST getValue()	{ return getChild(2); }
	
	public void setValue(AST value) { children.set(2, value); }
	
}
