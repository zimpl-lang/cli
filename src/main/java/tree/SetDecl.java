package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <table>
 * <tr> <td><b>set</b></td> 
 * 		<td>: 'set' sqRef</td><td>#SetDecl</td> </tr>
 * <tr> <td/> <td>| 'set' sqRef ':=' setExpr</td>
 * 		<td>#SetDefExpr</td></tr>
 * <tr> <td/> <td>| 'set' sqRef ':=' '{' readfile '}'</td>
 * 		<td>#SetDefRead</td></tr>
 * <tr> <td/> <td>;</td>
 * </table>
 * <p/>
 * The description above is the current one; the previous one is below.
 * <table>
 * <tr> <td><b>set</b></td> 
 * 		<td>: 'set' name=ID</td><td>#SetDecl</td> </tr>
 * <tr> <td/> <td>| 'set' name=ID ':=' setExpr</td>
 * 		<td>#SetDefExpr</td></tr>
 * <tr> <td/> <td>| 'set' name=ID ':=' '{' readfile '}'</td>
 * 		<td>#SetDefRead</td></tr>
 * <tr> <td/> <td>;</td>
 * </table>
 * <p/>
 * <b>NOTE:</b> indexed sets are not yet fully supported.
 * @author Javier MV
 * @since 0.2
 */
public class SetDecl extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.RULE_set, "set");
	
	public SetDecl(AST name, AST value) {
		super(token, Arrays.asList(name, value));
	// should be...
//	public SetDecl(SqRef sqRef, AST value) {
//		super(token, Arrays.asList(sqRef, value));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitSetDecl(this);
	}
	
	// redundant and convenient
	public String getName() { return getChild(0).token.getText(); }
	
	// redundant and convenient
	public AST getIndex() { return ((VarRef) getChild(0)).getIndex(); }
//	public AST getIndex() { return ((SqRef) getChild(0)).getIndex(); }
	
//	public SqRef getSqRef() { return (SqRef) getChild(0); }
	public VarRef getSqRef() { return (VarRef) getChild(0); }
	
	public AST getValue() { return getChild(1); }
	
	public void setValue(AST value) { children.set(1, value); }
	
}
