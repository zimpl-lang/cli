package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * <b>bound</b> : cmp ( expr | sign=('+'|'-')? 'infinity' ) ;<br/>
 * <b>cmp</b>	: '<' | '<=' | '!=' | '==' | '>=' | '>' ;
 * 
 * @author Javier MV
 * @since 0.2
 */
public class Bound extends AST {
	
	public Bound(Token cmp, AST value) {
		super(cmp, Arrays.asList(value));
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitBound(this);
	}
	
	public AST getComparative() { return getChild(0); }
	
}
