package tree;

import java.util.Arrays;

import org.antlr.v4.runtime.Token;

/**
 * Represents a reference to a function, also called a function call or
 * invocation. Associated rule: <br/>
 * 		&emsp; {@code fnRef : name=ID '(' args ')'}
 * TODO: update doc from new rule def
 * 
 * @author Javier MV
 * @since 0.3
 */
public class FnRef extends AST {
	
	// TODO: see VarRef's note
	public FnRef(Token id, AST args) { super(id, Arrays.asList(args)); }
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitFnRef(this);
	}
	
	public String getName() { return token.getText(); }
	
	// input? values? maybe explain the decision  in javadoc
	public AST getArgs() { return children.get(0); }
	
}
