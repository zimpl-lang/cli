package tree;

import org.antlr.v4.runtime.Token;

/**
 * @author Javier MV
 * @since 0.3
 */
public class SetEmpty extends AST {
	
	public static final Token token =
			AST.getToken(zimpl.ZimplParser.SET_EMPTY, "∅");
	
	public SetEmpty() { super(token); }
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitSetEmpty(this);
	}
	
}
