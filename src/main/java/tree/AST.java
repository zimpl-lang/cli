package tree;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.Tree;

/**
 * Abstract Syntax Tree; suitable for both rewrites and traversals.
 * This implementation is regular & heterogeneous (see [Parr2009]).<p/>
 * 
 * The following symbols are associated to terms without a clear text
 * representation: <br/>
 * { 𝑭 : formulation, 𝑴 : model, 𝑓 : function, ⇶ : mapping, ⇉ : chain,
 * → : duo, () : PAREN, <> : tuple, {} : setDesc, ∅ : SET_EMPTY }
 * 
 * @apiNote
 * Subclases are created when they simplify AST construction or Visitor
 * implementations. When possible, subclases reuse an existing Token,
 * rule number (as type) or Token text from ParseTree.
 * @implNote
 * If text is not available, subclasses define it; eg: Chain.<br/>
 * If rule is not available, grammar defines appropriate Tokens, be them
 * lex rules (eg: INFINITY) or Imaginary Tokens (eg: PAREN). <br/>
 * 
 * NOTE: future versions might benefit from a distiction between the
 * concepts of 'Formulation' (abstract, maybe partially defined) and
 * 'Model' (completely defined, ready to be solved).
 * 
 * @author Javier MV
 * @since 0.2
 */
public class AST implements Tree {
	
	protected static Map<Integer, Token> tokens = new HashMap<>();
	
	protected static Token getToken(int type, String text) {
		Token result = tokens.get(type);
		if (result != null) {
			if (!result.getText().equals(text)) {
				throw new IllegalArgumentException(); // FIXME: add type and text as msg
			}
			return result;
		}
		result = new CommonToken(type, text);
		tokens.put(type, result);
		return result;
	}
	
	AST parent = null;
	public Token token;
	
	public List<AST> children;
	// TODO: some analysis is required to drop this public visibility
//	List<AST> children;
	
	public AST(Token token) { this(token, (AST) null); }

	public AST(Token token, AST child) {
		this(token, child == null? null : Arrays.asList(child));
	}
	
	public AST(Token token, List<AST> children) {
		this.token = token;
		this.children = children;
	}
	
	public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visit(this);
	}
	
	public String toString() { 
		if (token == null) return "!";
		String text = token.getText();
		return text == null? "?" : text;
	}
	
	public String toStringTree() {
		if (children == null || children.isEmpty())
			return this.toString();
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		sb.append(this.toString());
		for (AST it : children) {
			sb.append(' ');
			// FIXME: currently, child can be 'null' for a hacky
			// reason: to distinguish between paramSingle & paramMany.
			// See AstBuilder.exitParameterMany().
			// NOTE: the note above might be outdated; see VarDecl's 
			// use of nulls for another possible use of 'null'.
			String child = (it == null)? "null" : it.toStringTree(); 
			sb.append(child);
		}
		sb.append(")");
		return sb.toString();
	}

	/** Narrows the return type; it's not an additional method. */
	@Override public AST getParent() { return parent; }

	/** Narrows the return type; it's not an additional method. */
	@Override public Token getPayload() { return token; }
	
	// NOTE: returning different concrete types eases further development
	// if they are not optional; eg: subclases usually define getName()
	// methods and their return type is often 'String'. See exception
	// in VarDecl's type.
	
	/** Narrows the return type; it's not an additional method. */
	@Override public AST getChild(int i) { return children.get(i); }
	
	public AST optGetChild(int i) {
		return getChildCount() <= i ? null : getChild(i);
	}
	
	@Override public int getChildCount() {
		return (children == null)? 0 : children.size();
	}
	
}
