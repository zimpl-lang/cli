package tree;

import org.antlr.v4.runtime.Token;

/**
 * This class represents a reference to a variable. It comes from the
 * rule fragment: <br/>
 * 		name=ID ('[' index ']')?
 * 
 * @author Javier MV
 * @since 0.3
 */
public class VarRef extends AST {
	
	// FIXME: indexedSet is mapped here; should rename class as 'SqRef'
	// FIXME: even when a param, BasicExpr 'ID[csv]' is mapped here.
	public VarRef(Token id, AST index) {
		super(id, index);
	}
	
	@Override public <T> T accept(Visitor<? extends T> visitor) {
		return visitor.visitVarRef(this);
	}
	
	public String getName() { return token.getText(); }
	
	public AST getIndex() { return optGetChild(0); }
	
}
