package tree;

/**
 * @param <T> Return type. Use 'Void' for no return type.
 * @author Javier MV
 * @since 0.2
 */
public interface Visitor<T> {
	T visit(AST node);		// TODO: should FormulationNode be created?
	T visitToken(AST node);
	T visitCommand(Command node); // FIXME: add tests
	T visitCheck(Check node); // FIXME: add tests
	T visitPrint(Print node); // FIXME: add tests
	T visitSetDecl(SetDecl node);
	T visitSetDesc(SetDesc node);
	T visitSetEmpty(SetEmpty node);
	T visitParam(Param node);
	T visitMapping(Mapping node);
	T visitChain(Chain node);
	T visitDuo(Duo node);
	T visitReadfile(Readfile node);	// FIXME: add tests
	T visitVarDecl(VarDecl node);
	T visitVarRef(VarRef node);
	T visitObjective(Objective node);
	T visitConstraint(Constraint node);
	T visitForall(Forall node);	// FIXME: add tests
	T visitFnDecl(FnDecl node);	// FIXME: add tests
	T visitFnRef(FnRef node);	// FIXME: add tests
	T visitOp(OpNode node);
	T visitParen(Paren node);
	T visitRange(Range node);
	T visitTuple(Tuple node);
	T visitSum(Sum node);
	T visitCondition(Condition node);
	T visitComparison(Comparison node);
	T visitBound(Bound node);
	T visitInfinity(Infinity node);
	T visitCsv(CSV node);	// FIXME: add tests
	T visitIf(If node);		// FIXME: add tests
}
