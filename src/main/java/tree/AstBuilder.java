package tree;

import static zimpl.ZimplParser.*;

import java.io.IOException;
import java.util.*;

import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import de.zib.zimpl.antlr.FormulationBaseListener;

import zimpl.Parsers;
import zimpl.UnderlineErrorListener;
import zimpl.ZimplParser;

/**
 * @author Javier MV
 * @since 0.2
 */
public class AstBuilder extends FormulationBaseListener {
	
	private static AST build(ParseTree tree) {
		AstBuilder builder = new AstBuilder();
		ParseTreeWalker.DEFAULT.walk(builder, tree);
		return builder.getAst();
	}
	
	public static AST fromFilename(String filename)
	throws IOException {
		// FIXME: move this 'file' features and error reporting to
		// ZimplParser.fromFile()
		ZimplParser parser = ZimplParser.fromFilename(filename);
		// TODO: check if removing existing (Collector) error listener
		// prevents detection of Lexer errors.
		parser.removeErrorListeners();
		parser.addErrorListener(new UnderlineErrorListener());
		// BailStrat forces stop before errorlistener reports errors
//		parser.setErrorHandler(new BailErrorStrategy());
		ParseTree tree;
		try {
			tree = parser.parse();
			List<String> errors = parser.getErrors();
			if (!errors.isEmpty()) // TODO: enhance this rudimentary impl
				throw new ParseCancellationException();
		} catch (ParseCancellationException e) {
			String detail = String.format(explainParseErrors(parser),
					filename);
			throw new ParseCancellationException(detail, e);
		}
		AST result;
		try {
			result = build(tree);
		} catch (Exception e) {
			String msg = "Unexpected problem while building AST";
			throw new RuntimeException(msg, e);
		}
		return result;
	}
	
	public static AST fromString(String text) {
		return fromString(text, RULE_formulation);
	}
	
	public static AST fromString(String text, int parseRule) {
		ZimplParser parser = ZimplParser.fromString(text);
		ParseTree tree = Parsers.applyRule(parser, parseRule);
		List<String> errors = parser.getErrors();
		if (!errors.isEmpty()) {
			String msg = "Parser errors found" + "\n" +
					"errors: " + errors.toString() + "\n" +
					"text: '" + text + "'\n" +
					"parse rule: " + parser.getRuleNames()[parseRule] + "\n" +
					"tree: " + tree.toStringTree(parser);
			throw new RuntimeException(msg);
		}
		return build(tree);
	}
	
	// FIXME: move method to another class. Maybe Parsers?
	// "%s" file reference seems awkward there. Maybe ZimplParser.fromFilename
	private static String explainParseErrors(ZimplParser zp) {
		List<String> errors = zp.getErrors();
		if (errors == null || errors.isEmpty()) {
			return "Reading of file '%s' has been cancelled.";
		}
		if (errors.size() == 1) {
			return "File '%s', " + errors.getFirst();
		}
		// case: errors.size() > 1
		String result = "Errors found while reading file '%s':";
		for (String err : errors)
			result += "\n" + "* " + err;
		return result;
	}
	
	
	Stack<AST> resultsStack = new Stack<>();
	
	public AST getAst() { 
		assert resultsStack.size() == 1;
		return resultsStack.peek(); 
	}
	
	AST ruleAST(RuleNode node) {
		assert node != null;
		return resultsStack.pop();
	}
	
	AST optRuleAST(RuleNode node) {
		return (node == null)? null : ruleAST(node);
	}
	
	AST terminalAST(TerminalNode node) {
		assert node != null;
		return new AST(node.getSymbol());
	}
	
	AST optTerminalAST(TerminalNode node) {
		return (node == null)? null : terminalAST(node);
	}
	
	AST tokenAST(Token token) {
		assert token != null;
		return new AST(token);
	}
	
	AST optTokenAST(Token token) {
		return (token == null)? null : tokenAST(token);
	}
	
	/** Utility method to help debugging. */
	void dumpResultsStack() {
		int i = 0;
		for (AST it : resultsStack)
			System.out.printf("%d: %s%n", i++, it.toStringTree());
	}
	
	@Override public void exitFormulation(FormulationContext ctx) {
		Token fToken = AST.getToken(RULE_formulation, "𝑭");
		AST result = new AST(fToken, new LinkedList<AST>());
		if (resultsStack.isEmpty()) { // Empty Formulation
			resultsStack.push(result);
			return;
		}
		Integer[] validTypesArray = { RULE_set, RULE_parameter,
				RULE_variable, GOAL, RULE_constraint, RULE_function,
				RULE_command, RULE_print, RULE_check
			};
		Set<Integer> validTypes = new HashSet<>(); 
		Collections.addAll(validTypes, validTypesArray);
		while (!resultsStack.isEmpty()) {
			// Formulation is popped in reverse order
			AST it = resultsStack.pop();
			int type = it.token.getType();
			if (!validTypes.contains(type)) {
				String msg = "Unexpected AST's Token type: " + type + "\n" +
						"it: " + it.toStringTree();
				// TODO: show type's symbolic name
				throw new RuntimeException(msg);
			}
			result.children.addFirst(it); // reverses stack order
		}
		resultsStack.push(result);
	}
	
	@Override public void exitCommand(CommandContext ctx) {
		AST cmd = null;
		if (ctx.print() != null) cmd = ruleAST(ctx.print());
		else if (ctx.check() != null) cmd = ruleAST(ctx.check());
		else throw new RuntimeException("Unexpected, this is");
		// @see exitConstraint()
		// next 'for' is a hack. see if antlr4 can produce λ (the empty
		// string) from tail recursion; it'd be nicer if 'forall' could
		// handle it's own nesting, at construction time, instead of
		// having to delay it for its context, here (and in 'do' command?)
		Forall forall = null;
		for (@SuppressWarnings("unused") var it : ctx.forall())
			forall = ((Forall) resultsStack.pop()).setNested(forall);
		resultsStack.push(new Command(forall, cmd));
	}
	
	@Override public void exitCheck(CheckContext ctx) {
		resultsStack.push(new Check(ruleAST(ctx.boolExpr())));
	}
	
	@Override public void exitPrint(PrintContext ctx) {
		resultsStack.push(new Print(ruleAST(ctx.csv())));
	}
	
	@Override public void exitSetDecl(SetDeclContext ctx) {
//		set :	'set' sqRef 					# SetDecl
		pushSetDecl(ctx.sqRef(), null);
	}
	
	@Override public void exitSetDefExpr(SetDefExprContext ctx) {
//		set : 'set' sqRef ':=' setExpr 			# SetDefExpr
		pushSetDecl(ctx.sqRef(), ctx.setExpr());
	}
	
	@Override public void exitSetDefRead(SetDefReadContext ctx) {
//		set : 'set' sqRef ':=' '{' readfile '}'	# SetDefRead
		resultsStack.push(new SetDesc(resultsStack.pop()));
		pushSetDecl(ctx.sqRef(), ctx.readfile());
	}
	
	void pushSetDecl(RuleNode name, RuleNode def) {
		AST value = optRuleAST(def); // mind reverse order 
		VarRef sqRef = (VarRef) ruleAST(name);
		resultsStack.push(new SetDecl(sqRef, value));
	}
	
	@Override public void exitSetExprBin(SetExprBinContext ctx) {
		resultsStack.push(popBinOp(ctx.op));
	}
	
	@Override public void exitSetExprParen(SetExprParenContext ctx) {
		resultsStack.push(new Paren(resultsStack.pop()));
	}
	
	@Override public void exitSetDescEmpty(SetDescEmptyContext ctx) {
//		setDesc: '{' '}' # SetDescEmpty | ...
		assert ctx.parent instanceof SetExprContext;
		resultsStack.push(new SetDesc(new SetEmpty()));
	}
	
	@Override public void exitSetDescStack(SetDescStackContext ctx) {
		resultsStack.push(new SetDesc(resultsStack.pop()));
	}

	@Override public void exitRange(RangeContext ctx) {
//		range:	lhs=expr '..' rhs=expr ;
		assert ctx.getChildCount() == 3;
		assert ctx.getChild(1).toString().equals("..");
		// reverse order
		AST rhs = resultsStack.pop();
		AST lhs = resultsStack.pop();
		resultsStack.push(new Range(lhs, rhs));
	}
	
	@Override public void exitParamDecl(ParamDeclContext ctx) {
//		:	'param' sqRef ( ':=' expr )?		# ParamDecl
		AST value = optRuleAST(ctx.expr());
		// FIXME: 'value' could be a single 'duo', without wrapping
		// 'chain' or 'mapping'. It should rather be wrapped, as it is
		// a kind of mapping and also (single elem) chain.
		
		// FIXME: rename VarRef -> SqRef
		VarRef sqRef = (VarRef) ruleAST(ctx.sqRef());
		// FIXME: Param should receive sqRef, not both "name" and "index"
		resultsStack.push(new Param(new AST(sqRef.getPayload()), sqRef.getIndex(), value));
	}
	
	@Override public void exitParamDef(ParamDefContext ctx) {
//		: 'param' sqRef ':=' mapping | ...
		AST mapping = ruleAST(ctx.mapping());
		// FIXME: rename VarRef -> SqRef
		VarRef sqRef = (VarRef) ruleAST(ctx.sqRef());
		// FIXME: Param should receive sqRef, not both "name" and "index"
		resultsStack.push(new Param(new AST(sqRef.getPayload()), sqRef.getIndex(), mapping));
	}
	
	@Override public void exitMapping(MappingContext ctx) {
//		: ( readfile | chain | readfile ',' chain) ('default' expr)? ;
		AST defaultValue = optRuleAST(ctx.expr());
		// FIXME: tests are missing; should also check ZimplVisitor
		Chain chain = (Chain) optRuleAST(ctx.chain());
		Readfile readfile = (Readfile) optRuleAST(ctx.readfile());
		resultsStack.push(new Mapping(readfile, chain, defaultValue));
	}
	
	@Override public void exitChain(ChainContext ctx) {
//		chain : ( duo (',' duo)* )? ;
		List<AST> duos = popList(ctx.duo().size(), Duo.class);
		resultsStack.push(new Chain(duos));
	}
	
	@Override public void exitDuo(DuoContext ctx) {
		AST elem = resultsStack.pop();
		Tuple tuple = (Tuple) resultsStack.pop();
		resultsStack.push(new Duo(tuple, elem));
	}
	
	@Override public void exitReadfile(ReadfileContext ctx) {
//		readfile: 'read' filename=STRING 'as' template=STRING readopts*;
//		readopts: 'skip' skip=INT | 'use' use=INT | 'match' match=STRING
//				| 'comment' comment=STRING ;
		AST filename = new AST(ctx.filename);;
		AST template = new AST(ctx.template);
		AST skip, use, match, comment;
		skip = use = match = comment = null;
		for (ReadoptsContext opt : ctx.readopts()) {
			switch (opt.start.getText()) {
			case "skip"	-> skip	= new AST(opt.skip);
			case "use"	-> use	= new AST(opt.use);
			case "match"-> match= new AST(opt.match);
			case "comment"->comment=new AST(opt.comment);
			default -> {
					String msg = "Unexpected value: " + opt.start.getText();
					throw new IllegalArgumentException(msg);
				}
			}
		}
		AST result = new Readfile(filename, template, skip, use, match, comment);
		resultsStack.push(result);
	}
	
	@Override public void exitVariable(VariableContext ctx) {
//		variable: 'var' sqRef VAR_TYPE? b1=bound? b2=bound? ;
		// pop from stack in reverse order
		List<BoundContext> bounds = ctx.bound();
		Bound bound1 = null, bound2 = null;
		assert 0 <= bounds.size() && bounds.size() <= 2;
		if (bounds.size() == 2)
			bound2 = (Bound) resultsStack.pop();
		if (bounds.size() > 0)
			bound1 = (Bound) resultsStack.pop();
//		VAR_TYPE : 'real' | 'binary' | 'integer' ;
		AST type = optTerminalAST(ctx.VAR_TYPE());
		// FIXME: rename VarRef -> SqRef
		VarRef sqRef = (VarRef) ruleAST(ctx.sqRef());
		AST result = new VarDecl(new AST(sqRef.token), sqRef.getIndex(), type, bound1, bound2);
		resultsStack.push(result);
	}
	
	@Override public void exitBound(BoundContext ctx) {
//		bound: cmp nExpr
//		cmp  : token=( '<' | '<=' | '!=' | '==' | '>=' | '>' ) ;
		AST nExpr = resultsStack.pop();
		resultsStack.push(new Bound(ctx.cmp().token, nExpr));
	}
	
	// TODO: see if class Infinity is still useful
//	@Override public void exitBoundInfinity(BoundInfinityContext ctx) {
////		bound: cmp sign=('+'|'-')? INFINITY # BoundInfinity | ...
////		cmp  : token=( '<' | '<=' | '!=' | '==' | '>=' | '>' ) ;
//		Infinity infy = new Infinity(ctx.sign);
//		AST result = new Bound(ctx.cmp().token, infy);
//		resultsStack.push(result);
//	}
//	
	@Override public void exitObjective(ObjectiveContext ctx) {
//		objective : GOAL name=ID? ':' expr;
//		GOAL : 'minimize' | 'maximize' ;
		AST expr = resultsStack.pop();
		AST name = optTerminalAST(ctx.ID());
		Token token = ctx.GOAL().getSymbol();
		resultsStack.push(new Objective(token, name, expr));
	}
	
	@Override public void exitConstraint(ConstraintContext ctx) {
//		constraint: 'subto' name=ID? ':' forall* comparison ;
		Comparison cmp = (Comparison) ruleAST(ctx.comparison());
		// @see exitCommand()
		// next 'for' is a hack. see if antlr4 can produce λ (the empty
		// string) from tail recursion; it'd be nicer if 'forall' could
		// handle it's own nesting, at construction time, instead of
		// having to delay it for its context, here (and in 'do' command?)
		Forall forall = null;
		for (@SuppressWarnings("unused") var it : ctx.forall())
			forall = ((Forall) resultsStack.pop()).setNested(forall);
		AST name = optTerminalAST(ctx.ID());
		resultsStack.push(new Constraint(name, forall, cmp));
	}
	
	@Override public void exitForall(ForallContext ctx) {
//		forall	: 'forall' condition sep=('do' | ':') ;
		Condition condition = (Condition) ruleAST(ctx.condition());
		resultsStack.push(new Forall(condition, ctx.sep));
	}
	
	@Override public void exitFunction(FunctionContext ctx) {
//		function: FN_TYPE fnRef (':=' expr)? ;
//		FN_TYPE : 'defnumb' | 'defstrg' | 'defbool' | 'defset' ;
//		fnRef	: name=ID '(' csv ')' ;
//		expr	: nExpr | strExpr | boolExpr | setExpr | tuple ;
		AST expr = optRuleAST(ctx.expr());
		FnRef fnRef = (FnRef) ruleAST(ctx.fnRef());
		AST fnType = terminalAST(ctx.FN_TYPE());
		// FIXME: FnDecl should receive fnRef instead of name + fnFnParams
		AST name = new AST(fnRef.token);
		// TODO: when FnDecl receives FnRef instead of ID + csv, the
		// problem of defining 'fnPARAMS' from 'fnARGS' will be resolved.
		// FIXME: it's not beautiful that 1) 'param' is assigned from
		// 'args', 2) that 'params' is used with a different meaning
		// from Model 'parameters'. 
		AST fnParams = fnRef.getArgs();
		FnDecl result = new FnDecl(fnType, name, fnParams, expr);
		resultsStack.push(result);
	}
	
	// NOTE: must be sorte for Arrays.binarySearch to work in exitNExpr
	static int[] knownUExprTypes = { ZimplParser.INFINITY,
			ZimplParser.INT, ZimplParser.FLOAT};
	// TODO: decide how to rewrite subtrees such as groups of same
	// operand (eg: +) are merged in a single node; eg: "3 + 2 + 1" ->
	// (+ 3 2 1). Maybe a Visitor is better suited than trying to peep
	// into children and their descendants.
	@Override public void exitNExpr(NExprContext ctx) {
		if (ctx.sign == null)
			return; // resultsStack already holds result
		
		// FIXME: "-1 + 2" is currently interpreted as (- (+ 1 2)) which
		// is semantically wrong. A new rewriting Visitor seems
		// convenient, instead of trying to fix here manually.
		// safety patch - begin
		AST child = resultsStack.pop();
		int childType = child.token.getType();
		int index = Arrays.binarySearch(knownUExprTypes, childType);
		if (index < 0) {
			String msg = "Child type " + childType + " not currently " +
					"supported in phrase: " + child.toStringTree();
			throw new RuntimeException(msg);
		}
		// safety patch - end
		List<AST> children = Arrays.asList(child);
		resultsStack.push(new OpNode(ctx.sign, children));
	}
	
	@Override public void exitUExpr(UExprContext ctx) {
//		: uExpr op=('*'|'/') uExpr | uExpr op=('+'|'-') uExpr | basicExpr ;
		if (ctx.basicExpr() != null)
			return; // resultsStack already holds result
		assert ctx.getChildCount() == 3;
		resultsStack.push(popBinOp(ctx.op));
	}
	
	@Override public void exitBasicExprToken(BasicExprTokenContext ctx) {
//		basicExpr : token=(INT | FLOAT)	# BasicExprToken | ...
		resultsStack.push(tokenAST(ctx.token));
	}
	
	@Override public void exitBasicExprParen(BasicExprParenContext ctx) {
//		basicExpr : '(' nExpr ')'		# BasicExprParen | ...
		resultsStack.push(new Paren(resultsStack.pop()));
	}
	
//	@Override public void exitSumExpr(SumExprContext ctx) {
	@Override public void exitRedExpr(RedExprContext ctx) {
//	//	sumExpr : 'sum' condition sep=('do' | ':') nExpr ;
//		redExpr :	op=('min'|'max'|'prod'|'sum') condition sep=('do'|':') nExpr ;
		assert ctx.getChildCount() == 4;
		AST expr = resultsStack.pop();
		String separator = ctx.getChild(2).getText();
		assert separator.equals(":") || separator.equals("do");
		Condition condition = (Condition) resultsStack.pop();
		resultsStack.push(new Sum(ctx.op, condition, ctx.sep, expr));
	}
	
	@Override public void exitCondition(ConditionContext ctx) {
//		: tuple 'in' setExpr (sep=('with' | '|') boolExpr)? ;
		AST predicate = optRuleAST(ctx.boolExpr());
		AST separator = optTokenAST(ctx.sep);
		AST setExpr = ruleAST(ctx.setExpr());
		assert ctx.getChild(1).getText().equals("in");
		Tuple tuple = (Tuple) ruleAST(ctx.tuple());
		AST result = new Condition(tuple, setExpr, separator, predicate);
		resultsStack.push(result);
	}
	
	@Override public void exitBoolExprNot(BoolExprNotContext ctx) {
		assert ctx.start.getText().equals("not");
		List<AST> children = Arrays.asList(resultsStack.pop());
		resultsStack.push(new OpNode(ctx.start, children));
	}
	
	@Override public void exitBoolExprBin(BoolExprBinContext ctx) {
		resultsStack.push(popBinOp(ctx.op));
	}
	
	@Override public void exitBoolExprToken(BoolExprTokenContext ctx) {
		resultsStack.push(new AST(ctx.token));
	}
	
	@Override public void exitBoolExprParen(BoolExprParenContext ctx) {
		resultsStack.push(new Paren(resultsStack.pop()));
	}
	
	@Override public void exitTuple(TupleContext ctx) {
		// FIXME: consider multi-item tuples
		// FIXME: add tests for this
		if (ctx.getChildCount() != 3) {
			System.err.println("Tuples with multiple elements are not " +
					"yet supported. Coming soon...");
			System.out.println(ctx.toStringTree());
		}
		assert ctx.getChild(0).toString().equals("<");
		assert ctx.getChild(1) != null;
		assert ctx.getChild(2).toString().equals(">");
		resultsStack.push(new Tuple(resultsStack.pop()));
	}
	
	/**
	 * FIXME: consider multiple bounds.
	 */
	@Override public void exitComparisonNExpr(ComparisonNExprContext ctx) {
//		// TODO: handle additional bounds
		assert ctx.bound().size() == 1;
//		comparison	: nExpr bound+ | lhs=strExpr cmp rhs=strExpr ;
//		bound		: cmp nExpr ;
//		cmp			: token=( '<' | '<=' | '!=' | '==' | '>=' | '>' ) ;
		Bound bound = (Bound) resultsStack.pop();
		AST rhs = bound.getComparative();
//		AST lhs = resultsStack.pop();
		AST lhs = ruleAST(ctx.nExpr());
		resultsStack.push(new Comparison(lhs, bound.token, rhs));
	}
	
	@Override public void exitComparisonStrExpr(ComparisonStrExprContext ctx) {
		AST rhs = ruleAST(ctx.rhs); // resultsStack.pop();
		AST lhs = ruleAST(ctx.lhs); // resultsStack.pop();
		resultsStack.push(new Comparison(lhs, ctx.cmp().token, rhs));
	}
	
	@Override public void exitSqRefCsv(SqRefCsvContext ctx) {
		// if csv.children.size == 1, resultsStack holds AST for 'elem',
		// not 'csv'.
		// TODO: test this!!!
		AST csv = optRuleAST(ctx.csv());
		// FIXME: VarRef should be called 'SqRef'
		// see exitBasicExpr, fnRef...
		resultsStack.push(new VarRef(ctx.name, csv));
	}
	
	@Override public void exitSqRefIndex(SqRefIndexContext ctx) {
//		sqRef: name=ID ('[' csv ']')? | name=ID '[' index ']' ;
//		index	: condition | setExpr ;
		// TODO: test this!!!
		AST index = ruleAST(ctx.index());
		// FIXME: VarRef should be called 'SqRef'
		// see exitBasicExpr, fnRef...
		resultsStack.push(new VarRef(ctx.name, index));
	}
	
	@Override public void exitFnRef(FnRefContext ctx) {
		// TODO: test this!!!
//		fnRef	:	name=ID '(' csv ')' ;
		AST args = ruleAST(ctx.csv()); // resultsStack.pop();
		resultsStack.push(new FnRef(ctx.name, args));
	}
	
	@Override
	public void exitCsv(CsvContext ctx) {
//		csv : ( expr (',' expr)* )? ;
		if (ctx.getChildCount() == 1)
			return;
		List<AST> exprs = popList(ctx.expr().size(), AST.class);
		resultsStack.push(new CSV(exprs));
	}
	
	@Override public void exitStrExprBin(StrExprBinContext ctx) {
		resultsStack.push(popBinOp(ctx.op));
	}
	
	@Override public void exitStrExprToken(StrExprTokenContext ctx) {
		resultsStack.push(terminalAST(ctx.STRING()));
	}
	
	@Override public void exitIfExpr(IfExprContext ctx) {
		AST elseExpr = ruleAST(ctx.elseExpr);
		AST thenExpr = ruleAST(ctx.thenExpr);
		AST boolExpr = ruleAST(ctx.boolExpr());
		resultsStack.push(new If(boolExpr, thenExpr, elseExpr));
	}
	
	
	
	<T extends AST> List<AST> popList(int size, Class<T> type) {
		if (size == 0)
			return null;
		List<AST> result = new ArrayList<>();
		for (int i = 0; i < size; ++i) {
			AST it = resultsStack.pop();
			if (!type.isInstance(it)) {
				String msg = String.format("Expected: %s, actual: %s", 
						type.getName(), it.getClass().getName());
//				throw new ClassCastException(msg);
				throw new IllegalArgumentException(msg);
			}
			result.addFirst(it); // reverses stack order
		}
		return result;
	}
	
	AST popBinOp(Token t) {
		// Remember stack's reverse order
		AST rhs = resultsStack.pop();
		AST lhs = resultsStack.pop();
		return new OpNode(t, Arrays.asList(lhs, rhs));
	}
	
}
