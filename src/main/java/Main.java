import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.antlr.v4.runtime.misc.ParseCancellationException;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Model.PositionalParamSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import picocli.CommandLine.Spec;
import tree.AST;
import zimpler.Reporter;
import zimpler.Resolver;
import zimpler.Solver;
import zimpler.Table;
import zimpler.io.Loader;
import zimpler.io.TextFileWriter;
import zimpler.io.XlsxWriter;
import zimpler.io.ZimplVisitor;

@Command(name = "zimpler", version = "zimpler 0.5", 
description = "Zimpl Expression Resolver. See https://gitlab.com" +
		"/zimpler-tool for info.", mixinStandardHelpOptions = true,
footer = {"Input data filename extensions:", "${exts}"})
public class Main implements Runnable {
	
	@Parameters(paramLabel = "<model>", defaultValue = "model.zpl",
			description = "Zimpl input model file.")
	String modelFilename;
	
	@Option(names = { "-o", "--output" }, paramLabel = "<file>",
			description = "Place the resolved model into <file>.")
	Path outPath;

	@Spec CommandSpec spec;
	
	@Override public void run() {
		// TODO: try running some mature cli tool, such as wget to get
		// additional usage information formats. Eg:
//		wget: missing URL
//		Usage: wget [OPTION]... [URL]...
//		Try `wget --help' for more options. [Then, in --help...]
		// Email bug reports, questions, discussions to <bug-wget@gnu.org>
		// and/or open issues at https://savannah.gnu.org/bugs/?func=additem&group=wget.
		AST resolvedModel = null;
		try {
			resolvedModel = resolveModel(modelFilename);
		} catch (NoSuchFileException e) {
			handleNoModelFileEx(e);
		} catch (IOException e) {
			// TODO: test for encoding, file access, isDirectory...
			System.err.println("Hiccup found");
			e.printStackTrace();
			System.exit(spec.exitCodeOnExecutionException());
		} catch (ParseCancellationException e) {
			// catches syntax errors
			System.err.println(e.getMessage());
			System.exit(spec.exitCodeOnExecutionException());
		} catch (RuntimeException e) {
			if (e.getCause() instanceof NoSuchFileException cause)
				// catches model.properties not found
				handleNoModelFileEx(cause);
			if (e.getCause() instanceof AccessDeniedException) {
				System.err.println(e.getMessage());
				System.exit(spec.exitCodeOnExecutionException());
			}
			if (e.getCause() instanceof NoSuchMethodException cause) {
				String causeMsg = cause.getMessage();
				if (causeMsg != null && 
						causeMsg.startsWith("zimpl.ZimplParser.")) {
					String msg = "Fatal error: can't find method '" + 
						causeMsg + "'; maybe this program was generated " +
						"with missing reflection metadata." + "\n" +
						"See BUILD.md for instructions." + "\n";
					System.err.println(msg);
					e.printStackTrace(); // log.debug...
					System.exit(spec.exitCodeOnExecutionException());
				}
			}
			if (e.getMessage() != null)
				System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(spec.exitCodeOnExecutionException());
		}
		
		// FIXME: what's next? should model be solved or merely output?
		// TODO: [usability] should output be overwritten?
		// TODO: [usability] should incomplete resolutions be treated as errors?
		
		try {
			Map<String, Double> solution = solveModel(resolvedModel);
			List<Table<Object>> report =
					createReport(resolvedModel, solution);
			XlsxWriter.write("result.xlsx", report);
		} catch (IOException e) {
			System.err.println("Hiccup found");
			e.printStackTrace();
			System.exit(spec.exitCodeOnExecutionException());
		}
	}
	
	static class ExtensionsBundle extends ResourceBundle {
		@Override protected Object handleGetObject(String key) {
			if (!key.equals("exts"))
				return null;
			int width = Resolver.knownLoaders.stream()
					.mapToInt(it -> it.acceptedExtension().length())
					.reduce(0, Integer::max) + 1;
			String result = (width == 0)? "  None found" : "" ;
			for (Loader<String> it : Resolver.knownLoaders) {
				String ext = "." + it.acceptedExtension();
				String padExt = String.format("%" + width + "s", ext);
				String fileFormat = "  " + padExt + "  " +
						it.extensionDescription();
				result += fileFormat + "\n";
			}
			return result;
		}
		@Override public Enumeration<String> getKeys() {
			return Collections.enumeration(List.<String>of("exts"));
		}
	}
	
	public static void main(String[] args) {
		int exitCode = new CommandLine(new Main())
				.setResourceBundle(new ExtensionsBundle())
				.execute(args);
		System.exit(exitCode);
	}
	
	AST resolveModel(String filename) throws IOException {
		return new Resolver().resolveFromFilename(filename);
	}
	
	void outputModel(AST model) throws IOException {
		String text = model.accept(new ZimplVisitor());
		// if outPath == null, result is printed to stdout
		if (outPath == null) {
			// TODO: instead of transforming the whole model to String
			// and sending it to stdout, an outStream might be given to
			// a Visitor that sends characters while traversing.
			System.out.println(text);
			return;
		}
		TextFileWriter.write(outPath, text);
	}
	
	Map<String, Double> solveModel(AST model) throws IOException {
		// Some solvers may need their model stored in a file, some may
		// be able to build their model through API. In addition, just
		// resolving is a noble ending.
		boolean shouldOutputTheModel = true;
		if (shouldOutputTheModel)
			outputModel(model);
		try {
			return new Solver().solve(model);
		} catch (IllegalStateException e) {
			// IllegalStateException is used to catch known and descriptive
			// error conditions, such as when CliOptimizer cannot find
			// 'solve.sh' or the script lacks exec permission. Other
			// Exception types are conditions not yet fully considered.
			System.err.println(e.getMessage());
			System.exit(spec.exitCodeOnExecutionException());
		}
		return null;
	}
	
	List<Table<Object>>
	createReport(AST model, Map<String, Double> sol)
	throws IOException {
		return new Reporter().createReport(model, sol);
	}
	
	void handleNoModelFileEx(NoSuchFileException e) {
		// if model was not specified as an argument, default value has
		// not been found and it seems harsh to call this an error.
		if (isModelSpecified())
			handleNoSuchFileEx(e);
		spec.commandLine().usage(System.out);
		System.exit(spec.exitCodeOnUsageHelp());
	}
	
	boolean isModelSpecified() {
		ParseResult pr = spec.commandLine().getParseResult();
		PositionalParamSpec fileParam = 
				spec.positionalParameters().getFirst();
		if (!fileParam.paramLabel().equals("<model>")) {
			String msg = "Expecting first positional parameter to be " +
					"<model>, found instead: " + fileParam.paramLabel();
			throw new RuntimeException(msg);
		}
		return pr.hasMatchedPositional(fileParam);
	}
	
	void handleNoSuchFileEx(NoSuchFileException e) {
//		e.printStackTrace();
		String filename = e.getMessage();
		String workdir = System.getProperty("user.dir");
		System.err.printf("Error: could not find '%s'%n", filename);
		System.err.println("Current working directory: " + workdir);
		System.exit(spec.exitCodeOnExecutionException());
	}
	
}
