package zimpler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Javier MV
 * @since 0.4
 */
public class Table <T> {
	
	public static class Builder <T> {
		
		Table<T> partial;
		
		public Builder(String sourceFilename) {
			partial = new Table<T>(sourceFilename); 
		}
		
		public void addRow(T[] row) { partial.rows.add(row); }
		
		// FIXME: make method private and call it implicitly, in order
		// to simplify and avoid clients from breaking class invariant.
		public void setFirstRowAsHeader() {
			if (partial.rows.isEmpty()) {
				String msg = "Table should not be empty";
				throw new IllegalStateException(msg);
			} else if (partial.rows.size() > 1) {
				String msg = "Table should only have 1 row now";
				throw new IllegalStateException(msg);
			}
			Map<String, Integer> header = new LinkedHashMap<>();
			T[] row = partial.rows.removeLast();
			for (int i = 0; i < row.length; ++i)
				header.put(row[i].toString(), i);
			partial.header.putAll(header);
		}
		
		/** Prevents further changes and narrows its type */
		public Table<T> getResult() {
			return new Table<T>(
					partial.source,
					Collections.unmodifiableMap(partial.header),
					Collections.unmodifiableList(partial.rows)
				);
		}
	}
	
	public static Map<String,String>
	adaptToPropertiesAsColumns(Table<String> table, String filename) {
		Map<String,String> result = new LinkedHashMap<>();
		for (String col : table.columnNames) {
			// Zimpl column indices start from 1
			int colIdx = table.header.get(col) + 1;
			// FIXME: allNumeral detection should be configurable.
			// TODO: could this be saved to avoid recomputation?
			boolean allNumeral =
					Resolver.areAllNumeral(table.getColumn(col));
			// \w: word char: [a-zA-Z_0-9]
			// \p{IsAlphabetic}: an alphabetic char (Unicode)
//			Pattern pattern = Pattern.compile("(\\w+)\\[(.*?)\\]"); // \w+[\w+]
//			Matcher matcher = pattern.matcher(col);
//			boolean isIndexed = matcher.matches();
			// TODO: should an existing index be trimmed? should colname
			// be reported without index?
//			System.out.println("isIndexed: " + isIndexed); // debug
			String colDesc = Resolver
					.encode(filename, colIdx, allNumeral);
//					.encode(table.source, colIdx, allNumeral);
			result.put(col, colDesc);
		}
		return result;
	}
	
	public static <T> Map<String,String>
	adaptToPropertiesAsTable(Table<String> table, String filename) {
		Map<String,String> result = new LinkedHashMap<>();
		int slashIdx = table.source.lastIndexOf('/');
		String name = (slashIdx == -1)?
			null :
			table.source.substring(slashIdx+1);
		
		int nameIdx = table.columnNames.indexOf("Name");
		// If there's a column named "Name", use filename or sheetname
		// as property name and column "Name", instead of whole table,
		// as its content.
		if (0 <= nameIdx) {
			List<String> col = table.getColumn("Name");
			// FIXME: allNumeral detection should be configurable.
			boolean allNumeral = Resolver.areAllNumeral(col);
			String descriptor = // Zimpl columns start from 1
					Resolver.encode(filename, nameIdx + 1, allNumeral);
			result.put(name, descriptor);
			return result;
		}
		// FIXME: doesn't call Resolver.encode() 'cause it doesn't
		// support multiple indices after ':' yet.
		String descriptor = "!" + filename + ":";
		int i = 1; // Zimpl cols start from 1
		for (String colname : table.columnNames) {
			List<String> col = table.getColumn(colname);
			// FIXME: allNumeral detection should be configurable.
			String nodesType = Resolver.areAllNumeral(col)? "n" : "s";
			descriptor += ((1 < i)? "," : "") + (i++) + nodesType;
		} // eg: "!filename:1s,2n"
		result.put(name, descriptor);
		return result;
	}
	
	
	public final String source;
	public final Map<String, Integer> header;
	public final List<T[]> rows;
	public final List<String> columnNames;
	
	/** Only Table.Builder should ever call this. */
	private Table(String sourceFilename) {
		this(sourceFilename, new LinkedHashMap<>(), new ArrayList<>());
	}
	
	public Table(String sourceFilename, Map<String, Integer> header,
			List<T[]> rows)
	{
		this.source = sourceFilename;
		this.header = header;
		this.rows = rows;
		String[] headers = new String[header.size()];
		header.forEach((k,v) -> headers[v] = k);
		this.columnNames = Arrays.asList(headers);
	}
	
	public List<T> getColumn(String name) {
		int index = header.get(name);
		return rows.stream().map( row -> row[index] ).toList();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.join(",", columnNames) + "\n");
		for (T[] row : rows) {
			String bracketsEnclosed = Arrays.toString(row);
			sb.append(bracketsEnclosed.substring(1, bracketsEnclosed.length()-1) + "\n");
		}
		return sb.toString();
	}
}
