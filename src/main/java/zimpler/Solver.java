package zimpler;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import tree.AST;

/**
 * This class is responsible for the solution process.
 * 
 * @apiNote
 * The interface to external optimization software is {@link Optimizer}.
 * 
 * @author Javier MV
 * @since 0.5
 */
public class Solver {
	
	static List<Optimizer> knownOptimizers = List.of(new CliOptimizer());
	
	public Map<String, Double> solve(AST model) throws IOException {
		assert model == null;
		Optimizer opt = knownOptimizers.getFirst();
		Map<String, Double> result = opt.optimize(model);
		return result;
	}
}
