package zimpler.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tree.*;
import zimpl.ZimplParser;

/**
 * This class generates textual representation of a Zimpl formulation.
 * 
 * @author Javier MV
 * @version 0.2
 */
public class ZimplVisitor extends AbstractVisitor<String> 
implements Visitor<String> {

	public static String visitFromFilename(String filename)
	throws IOException {
		return visitFromFilename(filename, new ZimplVisitor());
	}
	
	public static String visitFromString(String text) {
		return visitFromString(text, new ZimplVisitor());
	}
	
	public static String visitFromString(String text, int parseRule) {
		return visitFromString(text, new ZimplVisitor(), parseRule);
	}
	
	@Override public String visitToken(AST it) {
		return (it.token.getType() == ZimplParser.RULE_formulation) ?
				"" : it.token.getText();
	}
	
	@Override public String visitCommand(Command node) {
		return "do " + opt(node.getForall()) + fwd(node.getCmd()) + ";\n";
	}
	// TODO: it'd be nice to print a whitespace after ',' in 'print' commands
	
	@Override public String visitSetDecl(SetDecl node) {
		// FIXME: the empty set has null as token; this makes opt
		// return 'null' as String, an uncomfortable special case. 
		// Look for a better way to represent the empty set.
		// FIXME: this does not considers indexed sets.
		// FIXME: sqRef should be handled recursively; it's inlined as a
		// quick & dirty patch but this does not work with indexed sets.
		return "set " + fwd(node.getSqRef()) +
				inOpt(" := ", node.getValue(), "") + ";" + "\n";
	}
	
	@Override public String visitSetDesc(SetDesc node) {
		// TODO: make it configurable if there are whitespaces between
		// "{", "}" and the content. No whitespace is ok for {0,1} or
		// {0}+T but it may be preferable to use "{ read ... };". Should
		// it depends on the length of content?
		return "{" + fwd(node.getChild()) + "}";
	}
	
	@Override public String visitSetEmpty(SetEmpty node) { return " "; }
	
	@Override public String visitParam(Param node) {
		String index = inOpt("[", node.getIndex(), "]");
		return String.format("param %s%s%s;\n", node.getName(),
				index, inOpt(" := ", node.getValue(), ""));
	}

	@Override public String visitMapping(Mapping node) {
		String readfile = opt(node.getReadfile());
		String chain = opt(node.getChain());
		String defaultElem = preOpt(" default ", node.getDefault());
		if (!readfile.isBlank() && !chain.isBlank()) {
			throw new RuntimeException("Test this");
//			return String.format("%s, %s%s", readfile, chain, defaultElem);
		} else if (readfile.isBlank() && !chain.isBlank())
			return chain + defaultElem;
		else if (!readfile.isBlank() && chain.isBlank())
			return readfile + defaultElem;
		else if (readfile.isBlank() && chain.isBlank())
			return defaultElem;
		else throw new RuntimeException("Unexpected node: " + 
			node.toString());
	}
	
	@Override public String visitChain(Chain node) {
		return join(", ", node.getDuos());
	}
	
	@Override public String visitDuo(Duo node) {
		return String.format("%s %s", fwd(node.getTuple()),
				fwd(node.getElem()));
	}
	
	@Override public String visitReadfile(Readfile node) {
		String skip = preOpt(" skip ", node.getSkip());
		String use = preOpt(" use ", node.getUse());
		String match = preOpt(" match ", node.getMatch());
		String comment = preOpt(" comment ", node.getComment());
		return String.format("read %s as %s%s%s%s%s", 
				node.getFilename(), node.getTemplate(), skip, use, 
				match, comment);
	}
	
	@Override public String visitVarDecl(VarDecl node) {
		// TODO: see VarDecl.getType(); returning a String might not
		// be the best alternative, 'cause opt() doesn't work.
		String type = preOpt(" ", node.getType());
		String index = inOpt("[", node.getIndex(), "]"); 
		String bound1 = preOpt(" ", node.getBound1());
		String bound2 = preOpt(" ", node.getBound2());
		return String.format("var %s%s%s%s%s;\n", node.getName(),
				index, type, bound1, bound2);
	}
	
	@Override public String visitVarRef(VarRef node) {
		return node.getName() + inOpt("[", node.getIndex(), "]");
	}

	@Override public String visitObjective(Objective node) {
		return String.format("%s%s: %s;\n", visitToken(node),
				inOpt(" ", node.getName(), ""),
				opt(node.getExpression()));
	}
	
	@Override public String visitConstraint(Constraint node) {
//		constraint: 'subto' name=ID? ':' forall* comparison ;
		return "subto" + inOpt(" ", node.getName(), "") + ": " +
				inOpt("", node.getForall(), " ") + 
				fwd(node.getComparison()) + ";" + "\n";
	}
	
	@Override public String visitForall(Forall node) {
		// TODO: allow for configurable whitespace before colon.
		// TODO: preserve original whitespace, specially line breaks and
		// configurable reformating.
		return "forall " + fwd(node.getCondition()) + node.getSeparator() +
				" " + inOpt("\n" + "\t", node.getNested(), "");
	}
	
	@Override public String visitFnDecl(FnDecl node) {
		return String.format("%s %s(%s)%s;\n", node.getType(),
				node.getName(), opt(node.getIds()), inOpt(" := ",
						node.getDef(), ""));
	}
	
	@Override public String visitFnRef(FnRef node) {
		return node.getName() + "(" + fwd(node.getArgs()) + ")";
	}
	
	@Override public String visitOp(OpNode node) {
		String op = node.token.getText();
		if (node.getChildCount() == 1) { // 'not' boolExpr, '+/-' nExpr
			return op + (op.equals("not")? " " : "") + 
					fwd(node.getChild(0));
		}
		List<String> exprs =
				node.getValues().stream().map(this::fwd).toList();
		return String.join(op, exprs);
	}
	
	@Override public String visitParen(Paren node) {
		return "(" + fwd(node.getExpr()) + ")";
	}
	
	@Override public String visitRange(Range node) {
		return fwd(node.getLhs()) + ".." + fwd(node.getRhs());
	}
	
	@Override public String visitTuple(Tuple node) {
		return "<" + fwd(node.getCsv()) + ">";
	}
	
	@Override public String visitSum(Sum node) {
		return String.format("sum %s: %s", fwd(node.getCondition()),
				fwd(node.getExpr()));
	}
	
	@Override public String visitCondition(Condition node) {
		return String.format("%s in %s%s", fwd(node.getTuple()), 
				fwd(node.getSetExpr()),
				inOpt(" | ", node.getPredicate(), ""));
	}
	
	// FIXME: there's still a problem with 3-terms comparisons
	/** @see AstBuilder#exitComparison() */
	@Override public String visitComparison(Comparison node) {
		return String.format("%s %s %s", fwd(node.getLhs()),
				node.token.getText(), fwd(node.getRhs()));
	}
	
	@Override public String visitInfinity(Infinity node) {
		return String.format("%sinfinity", opt(node.getSign()));
	}
	
	@Override public String visitCsv(CSV node) {
		// FIXME: add tests for this!
		return join(",", node.getElems());
	}
	
	// TODO: would it be better to have a BasicExprNode?
	// AbstractVisitor seems good enough for INT, but there's some
	// problem with 'parentesis'; see hacky visit(AST node) above.
//	@Override public String visitBasicExpression(AST node) {...}
	
	@Override public String reduce(String current, String next, int i) {
		if (next == null)
			return current;
		if (current == null)
			return next;
		String sep = (current.isBlank() || current.endsWith("\n"))?
				"" : " ";
		return current + sep + next; 
	}

	String opt(AST node) { return inOpt("", node, ""); }
	
	String inOpt(String pre, AST node, String post) {
		return (node == null)? "" : pre + fwd(node) + post;
	}
	
	String inOpt(String pre, String mid, String post) {
		return (mid == null)? "" : pre + mid + post;
	}
	
	String preOpt(String pre, AST node) { return inOpt(pre, node, ""); }
	
	String preOpt(String pre, String str) { return inOpt(pre, str, ""); }

	String join(String separator, List<AST> children) {
		List<String> items = new ArrayList<>();
		for (AST it : children)
			items.add(fwd(it));
		return String.join(separator, items);
	}
	
}
