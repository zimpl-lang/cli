package zimpler.io;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author Javier MV
 * @since 0.4
 */
public class TextFileWriter {
	
	public static void
	write(Path path, String text) throws IOException {
		try (Writer writer = Files.newBufferedWriter(path)) {
			writer.write(text);
		} catch (Exception e) {
			// TODO: handle writer creation errors
			switch (e.getCause()) { // FIXME: parsing errors have raised
			// NullPointerException here	
			// FIXME-FIXME: parsing errors should not get here; they
			// should've been handled before.
			default -> {
				String msg = "Unexpected value: " + e.getCause();
				throw new IllegalArgumentException(msg);
				}
			}
		}
		
	}
	
	public static void 
	write(String filename, List<String> lines) throws IOException {
		// log.debug("about to write $filename with content: $lines")
		Path path = Paths.get(filename);
		try (Writer writer = Files.newBufferedWriter(path)) {
			for (String line : lines) {
				writer.write(line);
			}
		}
		
	}
	
}
