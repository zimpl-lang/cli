package zimpler.io;

import java.io.IOException;
import java.util.Map;

/**
 * @author Javier MV
 * @since 0.4
 */
public interface Loader <T> {
	
	Map<String, T> load(String filename) throws IOException;
	
	String acceptedExtension();
	
	String extensionDescription();
	
}
