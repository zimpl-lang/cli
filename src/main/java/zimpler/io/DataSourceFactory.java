package zimpler.io;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Javier MV
 * @since 0.4
 */
public class DataSourceFactory <T> {

	private Map<String, Loader<T>> loaders;
	
	public DataSourceFactory(List<Loader<T>> knownLoaders) {
		loaders = knownLoaders.stream().collect(Collectors
				.toMap(Loader::acceptedExtension, Function.identity())
			);
	}
	
	String extractFileExtension(String filename) {
		int extIdx = filename.lastIndexOf('.');
		if (extIdx == -1)
			return null;
		return filename.substring(extIdx + 1);
	}
	
	public Map<String,T> createFor(String modelname) {
		List<Path> candidates = findCandidates(modelname);
		Map<String,T> result = new LinkedHashMap<>();
		for (Path it : candidates) {
			String ext = extractFileExtension(it.toString());
//			System.out.println("ext: " + ext); // debug
			Loader<T> loader = loaders.get(ext);
			// TODO: catch IllegalArgumentException - if a malformed
			// Unicode escape appears in the input of Properties.load()
			// FIXME: this 'todo' is Properties specific; keep it there
			try {
				Map<String,T> p = loader.load(it.toString());
				// Contents of all files are merged
				if (p != null)
					result.putAll(p); // FIXME: should check for duplicates
			} catch (AccessDeniedException e) {
				String msg = "File '" + e.getMessage() + "' was found " +
						"but access is denied.";
				throw new RuntimeException(msg, e);
			} catch (IOException e) {
				System.err.println("e.class: " + e.getClass());
				String msg = "File " + e.getMessage() + "' was found " +
						"but it could not be read.";
				throw new RuntimeException(msg, e);
			} catch (RuntimeException e) {
				String msg = loader.getClass().getName() + " failed " +
						"to load " + it.toAbsolutePath().normalize();
				System.err.println(msg);
				if (e.getMessage() != null)
					System.err.println(e.getMessage());
				// FIXME: for debugging, it helps if 'e' is thrown.
				// Should a 'debug-mode' be configurable? Maybe using a
				// String 'msg' where e.printStackTrace(...) could be
				// redirected, log.debug(msg) could do the trick.
				throw e; // debug
			}
		};
		return result;
	}
	
	List<Path> findCandidates(String basename) {
		Set<String> knownExtensions = loaders.keySet();
		List<Path> candidates;
		Path basePath = Paths.get(basename).normalize();
		String lowerBasename =
				basePath.getFileName().toString().toLowerCase();
//		System.out.println("lowerBasename: " + lowerBasename); // debug
		Path workDir = basePath.getParent();
		if (workDir == null)
			workDir = Paths.get(".").normalize();
//		System.out.println("workdir: " + workDir); // debug
		try (Stream<Path> paths = Files.list(workDir)) {
			candidates = paths.filter( it -> {
				String lowIt = it.getFileName().toString().toLowerCase();
				String itExtension = extractFileExtension(lowIt);
				if (!knownExtensions.contains(itExtension))
					return false;
				// FIXME: it seems wasteful to return Paths that later
				// need to be associate with the same Loader that was
				// used to accept it in the first place.
				return lowIt.equals(lowerBasename + "." + itExtension);
			}).toList();
		} catch (InvalidPathException e) {
			// Paths.get(".") throws java.nio.file.InvalidPathException
			// Unchecked exception thrown when path string cannot be converted into a Path because the path string contains invalid characters, or the path string is invalid for other file system specific reasons.
			e.printStackTrace();
			candidates = List.of();
		} catch (IOException e) {
			// TODO: the following error handling should be moved to
			// another class, probably responsible of finding alternative
			// data sources. Resolution should be saved from error handling
			e.printStackTrace();
			candidates = List.of();
		}
		
		if (candidates.isEmpty()) {
			// FIXME: it's hard to tell what to do here. It may be the
			// case that resolution is based on element names, not the
			// model name, such as looking for an environment variable
			// to define an element, or that several basenames are 
			// evaluated as possible data sources (eg: tsp.props,
			// model.props, data.props).
			// In addition, there are different reasonable
			// expectations for the result; it might be expected that
			// a 'strict' strategy resolves every symbol, or that a
			// 'partial' strategy allows for partially unresolved
			// formulations and, then, not finding candidates might be
			// a feature, more than a failure, depending on context or
			// config.
			// As a quick and dirty solution, while this
			// problem is fully tackled, NoSuchFile is wrapped so that
			// it scales the stack without polluting every method's
			// signature.
//			String msg = "not in " + workDir.toAbsolutePath();
//			Throwable cause = 
//					new NoSuchFileException(basename, null, msg);
//			throw new RuntimeException(cause);
			String msg = "No data source found for basename '" +
					basename + "', with file extensions " + knownExtensions +
					", in " + workDir.toAbsolutePath() ;
			System.err.println(msg);
		}
		if (candidates.size() > 1)
			System.out.println("Reading multiple data files: " + candidates);
		
		return candidates;
	}
	
}
