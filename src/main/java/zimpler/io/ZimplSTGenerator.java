package zimpler.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import de.zib.zimpl.antlr.FormulationParser;
import tree.*;
import zimpl.ZimplParser;

/**
 * This class uses the StringTemplate library to generates a textual
 * representation of a Zimpl formulation. <p/>
 * <b>NOTE:</b> This version is limited but functional. There's an
 * alternative Visitor implementation that doesn't use StringTemplate.
 * 
 * @see ZimplVisitor
 * @author Javier MV
 * @since 0.2
 */
public class ZimplSTGenerator {
	
	static STGroup templates = new STGroupFile("zimpl.stg");
	
	public static String fromFilename(String filename)
	throws IOException {
		AST ast = AstBuilder.fromFilename(filename);
		return ZimplSTGenerator.generate(ast);
	}
	
	public static String fromString(String text) {
		AST ast = AstBuilder.fromString(text);
		return ZimplSTGenerator.generate(ast);
	}
	
	public static String generate(AST formulation) {
		if (formulation.getChildCount() == 0)
			return "";
		StringBuilder sb = new StringBuilder();
		List<AST> statementGroups = formulation.children;
		// Statement groups: Parameters, Sets, Variables, Objectives, Constraints
		for (AST group : statementGroups) {
			int type = group.token.getType();
			if (type == ZimplParser.RULE_set) {
				sb.append(generateSet((SetDecl) group));
			} else if (type == ZimplParser.RULE_parameter) {
				sb.append(generateParameter((Param) group));
			} else if(type == ZimplParser.RULE_variable) {
				sb.append(generateVarDecl((VarDecl) group));
			} else if (type == ZimplParser.GOAL) {
				sb.append(generateObjective((Objective) group));
			} else if (type == ZimplParser.RULE_constraint) {
	//			System.out.println("gen.it: " + it.toStringTree());
				String gen = generateConstraint((Constraint) group);
	//			System.out.println("gen.txt: " + gen);
				sb.append(gen);
			}
			else {
				System.err.println("ZimplSTGenerator.node: " + group.toStringTree());
				String msg = "Unexpected token type: " + type;
				throw new IllegalArgumentException(msg);
			}
		}
		return sb.toString();
	}
	
	static String generateSet(SetDecl node) {
//		set(it, value) ::= "set <it.name><setInit(value)>;<\n>"
//		setInit(v) ::= "<if(v)> := { <opt(v)>}<endif>"
		ST st = templates.getInstanceOf("set");
		st.add("it", node);
		AST value = node.getValue();
		if (value == null) // undefined set
			return st.render();
		assert value.token != null;
		int type = value.token.getType();
		if (type == ZimplParser.RULE_setDesc) {
			String desc = generateSetDesc((SetDesc) value);
			st.add("value", desc);
			return st.render();
		}
		System.err.println("ZimplSTGenerator.generateSet()...");
		System.err.println("value.token: " + value.token.getText());
		System.err.println("value.token.type: " + value.token.getType());
		throw new RuntimeException("not implemented yet!");
	}
	
	static String generateSetDesc(SetDesc node) {
		ST st = templates.getInstanceOf("setDesc");
		AST child = node.getChild();
		int type = child.token.getType();
		String value;
		if (type == ZimplParser.SET_EMPTY)
			value = " ";
		else if (type == ZimplParser.RULE_csv)
			value = generateCsv((CSV) child);
		else if (type == ZimplParser.RULE_range) {
			ST range = templates.getInstanceOf("range");
			range.add("it", child);
			value = range.render();
		} else if (type == ZimplParser.RULE_condition)
			value = generateCondition((Condition) child);
		else {
			System.err.println(node.toStringTree());
			String msg = "node.token.type not implemented yet: " + type; // FIXME
			throw new IllegalArgumentException(msg);
		}
		st.add("it", value);
		return st.render();
	}
	
	static String generateParameter(Param node) {
		ST st = templates.getInstanceOf("parameter");
//		parameter(it, value) ::= "param <it.name><index(it.index)><init(value)>;<\n>"
		// index(idx) ::= "<if(idx)>[<idx>]<endif>"
		// init(v) ::= "<if(v)> := <v><endif>"
		// FIXME: 'index' only works for terminals 
		st.add("it", node);
		if (node.getIndex() == null) // unindexed parameter
			st.add("value", node.getValue());
		else {
			st.add("value", generateMapping((Mapping) node.getValue()));
		}
		return st.render();
	}
	
	static String generateVarDecl(VarDecl node) {
		ST st = templates.getInstanceOf("var");
		st.add("it", node);
		// FIXME: 'index' only works for terminals 
		st.add("bound", generateBound(node.getBound1()));
		st.add("bound", generateBound(node.getBound2()));
		return st.render();
	}

	static String generateBound(Bound node) {
		if (node == null)
			return null;
//		bound(cmp, cive)::= "<if(cmp)> <cmp> <cive><endif>"
		ST st = templates.getInstanceOf("bound");
		st.add("cmp", node.token.getText());
		String cive = generateComparative(node.getComparative());
		st.add("cive", cive);
		return st.render();
	}
	
	static String generateComparative(AST node) {
		return switch (node) {
			case Infinity inf -> (
					(inf.getSign() == null)? "" : 
						inf.getSign().toString()
					) + "infinity";
			default -> 
				generateExpression(node);
			};
	}
	
	static String generateObjective(Objective node) {
		ST st = templates.getInstanceOf("objective");
		String expr = generateExpression(node.getExpression());
		st.add("it", node);
		st.add("expr", expr);
		return st.render();
	}
	
	static String generateConstraint(Constraint node) {
//		::= "subto <opt(it.name)>: <expr>;<\n>"
		ST st = templates.getInstanceOf("constraint");
		String forall = generateForall(node.getForall());
		String comparison = generateComparison(node.getComparison());
		st.add("it", node);
		st.add("forall", forall);
		st.add("comparison", comparison);
		return st.render();
	}
	
	static String generateForall(Forall node) {
		if (node == null)
			return null;
//		::= "forall <condition>: <child>"
		ST st = templates.getInstanceOf("forall");
		String condition = generateCondition(node.getCondition());
//		String child = generateForallOrComparison(node.getChild());
		String child = generateForall(node.getNested());
		st.add("condition", condition);
		st.add("child", child);
		return st.render();
	}
	
	/** 
	 * @see AstBuilder#exitComparison()
	 */
	static String generateComparison(Comparison node) {
		// FIXME: handle optional cmp + expression 
		assert node.getChildCount() == 2;
		ST st = templates.getInstanceOf("comparison");
		String lhs = generateExpression(node.getLhs());
		String cmp = node.token.getText();
		String rhs = generateExpression(node.getRhs());
		st.add("lhs", lhs);
		st.add("cmp", cmp);
		st.add("rhs", rhs);
		return st.render();
	}
	
	static String generateExpression(AST node) {
		if (node instanceof OpNode)
			return generateOpExpression((OpNode) node);
		return generateBasicExpression(node);
	}
	
	static String generateOpExpression(OpNode node) {
		assert node.getChildCount() > 1;
		List<String> exprs = new ArrayList<>();
		for (AST expr : node.children)
			exprs.add(generateExpression(expr));
		String op = node.token.getText();
		return String.join(op, exprs);
	}
	
	static String generateBasicExpression(AST node) {
//		basicExpr	: ID ('[' csv ']')? | INT | FLOAT | sumExpression
//					| fnRef | '(' expr ')' ;
		if (node instanceof VarRef) // aka SqRef
			return generateVarRef((VarRef) node);
		if (node instanceof Sum) // aka Red
			return generateRedExpression((Sum) node);
		int type = node.token.getType();
		if (type == FormulationParser.ID) {
			assert node.getChildCount() == 1;
			String name = node.token.getText();
			AST index = node.getChild(0);
			ST st = templates.getInstanceOf("index");
			st.add("idx", index);
			return name + st.render();
		} else if (type == FormulationParser.INT) {
			return node.toString();
		} else if (type == ZimplParser.PAREN) {
			assert node.token.getText().equals("()");
			assert node.getChildCount() == 1;
			ST st = templates.getInstanceOf("paren");
			String expr = generateExpression(node.getChild(0));
			st.add("expr", expr);
			return st.render();
		}
		System.err.println(node.toStringTree());
		String msg = "node.token.type not implemented yet: " + type; // FIXME
		throw new IllegalArgumentException(msg);
	}
	
	static String generateSetExpression(AST node) {
//		: setExpression '*' setExpression | setDefinition | ID ;
		int type = node.token.getType();
		if (type == FormulationParser.ID) {
			return node.token.getText();
		}
		System.err.println("genSetExpr: " + node.toStringTree());
		throw new RuntimeException("not implemented yet!");
	}
	
	static String generateRedExpression(Sum node) {
		String condition = generateCondition(node.getCondition());
		String expr = generateExpression(node.getExpr());
		ST st = templates.getInstanceOf("sum");
		st.add("condition", condition);
		st.add("expr", expr);
		return st.render();
	}
	
	static String generateCondition(Condition node) {
		ST st = templates.getInstanceOf("criterion");
		// FIXME: getTuple() works for single elems only
		// tuple.children: ['<', csv, '>']
		st.add("atom", node.getTuple().getCsv());
		st.add("setExpr", generateSetExpression(node.getSetExpr()));
		return st.render();
	}
	
	static String generateMapping(Mapping node) {
		if (node == null)
			return null;
		ST mapSt = templates.getInstanceOf("mapping");
		Chain chain = node.getChain();
		for (int i = 0; i < chain.getChildCount(); ++i) { // NOTE different increment
			ST iSt = templates.getInstanceOf("duo");
			Duo duo = (Duo) chain.getChild(i);
			iSt.add("k", generateTuple(duo.getTuple()));
			iSt.add("v", generateExpression(duo.getElem()));
			mapSt.add("e", iSt.render());
		}
		return mapSt.render(); 
	}
	
	static String generateVarRef(VarRef node) {
		ST st = templates.getInstanceOf("sqRef");
		st.add("it", node);
		return st.render();
	}
	
	static String generateTuple(Tuple node) {
		ST st = templates.getInstanceOf("tuple");
		st.add("elem", node.getCsv());
		return st.render();
	}
	
	static String generateCsv(CSV node) {
		ST st = templates.getInstanceOf("csv");
		st.add("it", node);
		return st.render();
	}
	
}
