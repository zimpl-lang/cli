package zimpler.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.dhatim.fastexcel.Workbook;
import org.dhatim.fastexcel.Worksheet;

import zimpler.Table;

public class XlsxWriter {
	
	public static void
	write(String filename, List<Table<Object>> sheets) throws IOException {
		// log.debug("about to write $filename with content: $lines")
//		for (Table<Object> sheet : sheets) {				// DEBUG
//			System.out.println("sheet:" + sheet.source);	// DEBUG
//			System.out.println(sheet);						// DEBUG
//		}
		// from https://github.com/dhatim/fastexcel
		try (OutputStream os = new FileOutputStream(filename);
				// FIXME: get AppName & AppVersion from Main
				Workbook wb = new Workbook(os, "zimpler", "0.5")) {
			for (Table<Object> sheet : sheets.reversed()) {
				Worksheet ws = wb.newWorksheet(sheet.source);
				for (int i = 0; i < sheet.columnNames.size(); ++i)
					ws.value(0, i, sheet.columnNames.get(i));
				for (int j = 0; j < sheet.rows.size(); ++j) {
					Object[] row = sheet.rows.get(j);
					for (int i = 0; i < sheet.columnNames.size(); ++i) {
						if (row[i] instanceof String)
							ws.value(j+1, i, (String) row[i]);
						else if (row[i] instanceof Double)
							ws.value(j+1, i, (Double) row[i]);
						else {
							String msg = "Unexpected type: " + 
									row[i].getClass();
							throw new IllegalArgumentException(msg);
						}
					}
				}
				// Create a freeze pane (1st row and no columns will be
				// kept still while scrolling).
				ws.freezePane(0, 1);
			}
		} catch (FileNotFoundException e) {
			// TODO: see new FileOutpuStream() exceptions
			throw e;
		}
	}
	
}
