package zimpler.io;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

/**
 * @author Javier MV
 * @since 0.4
 */
public class PropertiesLoader implements Loader<String> {
	
	public static final 
	String DEFAULT_PROPERTIES_FILENAME = "model.properties"; 
	
	public static Map<String, String> getDefaultModelProperties() {
		try {
			return new PropertiesLoader()
					.load(DEFAULT_PROPERTIES_FILENAME);
		} catch (Exception e) {
			String msg = "Could not load default Properties from " +
					DEFAULT_PROPERTIES_FILENAME;
			throw new RuntimeException(msg, e);
		}
	}
	
	@Override public String acceptedExtension() { return "properties"; }

	@Override public String extensionDescription() {
		return "Text file with string key-value pairs";
	}
	
	public Map<String, String> load(String filename) throws IOException {
		Properties props = new Properties();
		try (Reader reader = Files.newBufferedReader(Paths.get(filename))) {
			props.load(reader);
		}
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Map<String, String> result = (Map) props;
		return result;
	}
	
}
