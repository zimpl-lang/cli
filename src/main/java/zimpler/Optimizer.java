package zimpler;

import java.io.IOException;
import java.util.Map;

import tree.AST;

/**
 * This interface represents programs that implement optimization
 * algorithms such as Simplex or Branch and Cut. They take a Mixed
 * Integer Linear Program (MILP) as input and return an assignment of
 * values to variables.<br/>
 * 
 * @apiNote
 * Another name for this concept is <i>Code</i>, though not in vogue at
 * the moment. The term <i>Solver</i> was reserved for the class
 * responsible for the solution process. A different name was chosen,
 * not to confuse the concepts they relate to.
 * 
 * @author Javier MV
 * @since 0.5
 * @see <a href="https://en.wikipedia.org/wiki/Simplex_algorithm">
 * 		Simplex algorithm</a>
 * @see <a href="https://en.wikipedia.org/wiki/Branch_and_cut">
 * 		Branch and Cut algorithm</a>
 * @see <a href="https://en.wikipedia.org/wiki/Integer_programming">
 * 		Mixed-Integer Linear Programming</a>
 */
public interface Optimizer {
	
	Map<String, Double> optimize(AST model) throws IOException;
	
	boolean isEnabled();
	
	String getDescription();
	
}
