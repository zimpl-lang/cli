package zimpler;

import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Javier MV
 * @since 0.4
 */
public class Graph {
	
	public static enum Type { DIRECTED, UNDIRECTED }
	
	public static record Edge(String source, String target) { }
	
	/** Represents an updatable and partially built Graph. */
	public static class Builder {
		
		String name;
		Type type;
		Graph partial;
		
		public Builder(String sourceFilename) {
			partial = new Graph(sourceFilename);
		}
		
		public String addNode(String id) {
			partial.nodes.add(id);
			return id;
		}
		
		public Edge addEdge(String source, String target) {
			Edge result = new Edge(source, target);
			partial.edges.add(result);
			return result;
		}
		
		/** Prevents further changes and narrows its type */
		public Graph getResult() {
			return new Graph(partial.source, type, name,
					Collections.unmodifiableSet(partial.nodes),
					Collections.unmodifiableSet(partial.edges),
					Collections.unmodifiableMap(partial.edgeAttrs)
				);
		}
		
		public void addEdgeAttr(Edge e, String attrName, String attrValue) {
			Map<Edge, String> attrMap = partial.edgeAttrs.get(attrName);
			if (attrMap == null)
				attrMap = new LinkedHashMap<>();
			attrMap.put(e, attrValue);
			partial.edgeAttrs.put(attrName, attrMap);
		}
		
		public void setName(String graphName) { name = graphName; }
		
		public void setType(Type t) { type = t; }
		
	}
	
	// TODO: add nodeAttrs and (GRAPH | NODE | EDGE) attr_list
	
	public final String source;
	public final Graph.Type type;
	public final String name;
	public final Set<String> nodes;
	public final Set<Edge> edges;
	
	/** The 1st String is the attribute name; the 2nd String is the
	 * value associated to the Edge.
	 */
	public final Map<String, Map<Edge, String>> edgeAttrs;
	
	/** Only Graph.Builder should ever call this. */
	private Graph(String sourceFilename) {
		this(sourceFilename, null, null, new LinkedHashSet<>(),
				new LinkedHashSet<>(), new LinkedHashMap<>());
	}
	
	public Graph(String sourceFilename, Type t, String graphName, 
			Set<String> graphNodes, Set<Edge> graphEdges, 
			Map<String, Map<Edge, String>> graphEdgeAttrs)
	{
		source = Paths.get(sourceFilename).normalize().toString();
		type = t;
		name = graphName;
		nodes = graphNodes;
		edges = graphEdges;
		edgeAttrs = graphEdgeAttrs;
	}
	
}
