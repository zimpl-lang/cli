package zimpler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.Token;

import de.zib.zimpl.antlr.FormulationParser;
import tree.AST;
import tree.AbstractVisitor;
import tree.AstBuilder;
import tree.Param;
import tree.SetDecl;
import tree.SetDesc;
import tree.Visitor;
import zimpler.io.PropertiesLoader;

/**
 * This class looks for concrete values to assign to input data
 * elements that are declared but not defined.
 * 
 * @author Javier MV
 * @since 0.2
 */
public class ResolverVisitor extends AbstractVisitor<AST> 
implements Visitor<AST> {
	
	public static AST visitFromFilename(String filename)
	throws IOException {
		return visitFromFilename(filename, new ResolverVisitor());
	}
	
	public static AST visitFromString(String text) {
		return visitFromString(text, new ResolverVisitor());
	}
	
	Map<String,String> props; // FIXME: please rename this
	Properties alias;
	
	/**
	 * FIXME: this is a basic approach to problem reporting; a better
	 * solution might log the data files read or the original input
	 * file where the unresolved element was found.
	 */
	public List<AST> unresolved = new ArrayList<>();
	
	/**
	 * Loads default Properties file, if it exists.
	 */
	public ResolverVisitor() {
		this(PropertiesLoader.getDefaultModelProperties(), null);
	}
	
	public ResolverVisitor(Map<String, String> props, Properties alias) {
		this.props = props;
		this.alias = (alias == null)? new Properties() : alias;
	}
	
	@Override public AST visitToken(AST it) { return it; }
	
	@Override public AST visitParam(Param node) {
		if (node.getValue() != null)
			return node;
		
		String key = node.getName();
		// NOTE: reading tables, the column name may be the element's 
		// 'primary' name (without index), as if unindexed.
		String value = props.get(key);
		AST index = node.getIndex();
		if (value == null && alias.containsKey(key))
			value = props.get(alias.getProperty(key));
		if (value == null && index != null) {
			// FIXME: using 'node.getIndex().toString()' only works for
			// ID indices, not for more complex SetExpr such as "I*J" or
			// "x[<i> in { 1..8 } with i mod 2 == 0]".
			// TODO: it will be necessary to parse the index; see 
			// below for 'mapping' ('pairList') parsing.
			key += "[" + index + "]";
			value = props.get(key);
		}
		if (value == null && alias.containsKey(key))
			value = props.get(alias.getProperty(key));
		if (value == null) {
			// TODO: collect unresolvable params
//				String msg = "No value found for parameter: " + 
//						node.getName();
//				System.err.println(msg);
			unresolved.add(node);
			return node;
		}
		AST result;
		// FIXME: handle 'readfile' value.
		if (index == null) {
			// TODO test for different value Types (Float, String, Tuple?)
			Token token  = new CommonToken(FormulationParser.INT, value);
			result = new AST(token);
			node.setValue(result);
			return node;
		}
		
		if (index.getChildCount() > 0) {
			List<AST> children = new ArrayList<>();
			for (int i = 0; i < index.getChildCount(); ++i)
				children.add(index.getChild(i));
			String msg = "At the moment, only simple (childless) " + 
				"indices are supported; found " + index.getChildCount() + 
				" children: " + children;
			throw new RuntimeException(msg);
		}
		// FIXME: this is a simplification; 'index' should be
		// traversed for individual domain keys & descs
		String idxKey = index.toString();
		String idxDesc = props.get(idxKey);
		if (idxDesc == null) { 
			// this may happen, eg, if loading indexed param from
			// properties files, instead of csv
			int parseRule = FormulationParser.RULE_mapping;
			result = AstBuilder.fromString(value, parseRule);
		} else {
			value = Resolver.getReadPhrase(value, idxDesc);
			int parseRule = FormulationParser.RULE_mapping;
			result = AstBuilder.fromString(value, parseRule);
		}
		node.setValue(result);
		return node;
	}
	
	@Override public AST visitSetDecl(SetDecl node) {
		// TODO: consider indexed sets for key definition
		if (node.getValue() != null)
			return node;
		
		String key = node.getName();
		// FIXME: the line below is a quick & dirty hack; sqRef should
		// be handled recursively. This does not work with indexed sets.
//		String key = node.getSqRef().token.getText();
		
		String value = props.get(key);
		if (value == null && alias.containsKey(key))
			value = props.get(alias.getProperty(key));
		if (value == null) {
			// TODO: collect unresolvable sets
//			String msg = "No value found for set: " + 
//					node.getName();
//			System.err.println(msg);
			unresolved.add(node);
			return node;
		}
		
		if (Resolver.isDescriptor(value))
			value = "{" + Resolver.getReadPhrase(null, value) + "}";
		// pattern ~= '{' 'read' ... '}'
		Pattern readSet = Pattern.compile("\\s*\\{\\s*(read.*?)\\}\\s*");
		Matcher matcher = readSet.matcher(value);
		AST result;
		// TODO: consider indexed sets for value resolution.
		if (matcher.find()) {
//			'set' sqRef ':=' '{' readfile '}' | ...
			String text = matcher.group(1);
			int parseRule = FormulationParser.RULE_readfile;
			result = new SetDesc(AstBuilder.fromString(text, parseRule));
		} else {
//			'set' sqRef ':=' setExpr | ...
			int parseRule = FormulationParser.RULE_setExpr;
			result = AstBuilder.fromString(value, parseRule);
		}
		// TODO: should check if 'result' is an actual AST and if text
		// has been fully consumed. Errors are checked for in AstBuilder
		node.setValue(result);
		return node;
	}
	
	@Override public AST reduce(AST current, AST next, int i) {
		return reduceRewriteAst(current, next, i);
	}
	
}
