package zimpler;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import tree.AST;
import tree.Visitor;
import tree.Visitors;
import zimpler.io.CsvLoader;
import zimpler.io.DataSourceFactory;
import zimpler.io.DotLoader;
import zimpler.io.Loader;
import zimpler.io.PropertiesLoader;
import zimpler.io.XlsxLoader;

/**
 * This class is responsible of coordinating the resolution process.
 * It delegates AST traversal and rewriting to {@code ResolverVisitor},
 * but handles initialization and defines useful concepts and methods.
 * Of special interest is the {@code Descriptor} encoding of
 * Strings; it binds a filename with column names from a tabular data
 * source. {@code Descriptor}s are generated when data is read by
 * {@code Loaders} and they are transformed later into AST phrases. <p/>
 * 
 * The concrete encoding of a {@code Descriptor} is:<br/>
 * 		&emsp; '!' + {@code filename} + ':' + {@code colnames} <br/>
 * where {@code colnames} is: <br/>
 * 		&emsp; ('s'|'n') '+' | {@code c1}(','{@code ci})*, <br/>
 * 's' and 'n' are Zimpl column datatypes refering to String and Number,
 * and {@code c*} is: <br/>
 * 		&emsp; {@code idx} ('s'|'n) <br/>
 * with {@code idx} a column index, starting from 1.<p/>
 * 
 * Note that, acording to {@code Properties} JavaDoc, characters '!' and
 * '#' define comment lines in {@code Properties} files and are, 
 * therefore, invalid key prefixes.
 * 
 * @author Javier MV
 * @since  0.4
 * @see    <a href="https://docs.oracle.com/en/java/javase/21/docs/api/
java.base/java/util/Properties.html#load(java.io.Reader)">
 * 			Java 21 API Documentation of Properties</a>
 */
public class Resolver {
	
	// TODO: evaluate if Descriptor should rather be a class on its own
	// with the current string as its internal representation
	static boolean isDescriptor(String s) {
		if (s.charAt(0) != '!')
			return false;
		int sep = s.lastIndexOf(":");
		if (sep == -1)
			return false;
		String suffix = s.substring(sep + 1);
		// pattern ~= [sn] '+' | (\d+ [sn] (',' \d+ [sn])* )
		return suffix.matches("[sn]\\+|\\d+[sn](,\\d+[sn])*");
	}
	
	static void checkIsDescriptor(String s) {
		if (isDescriptor(s))
			return;
		String msg = "Descriptor expected, found '" + s + "'. " +
				"Descriptors begin with the '!' char and end with ':' " +
				"followed by a Zimpl column descriptor, eg: '1n'.";
		throw new IllegalArgumentException(msg);
	}
	
	static String[] decode(String descriptor) {
		checkIsDescriptor(descriptor);
		int sep = descriptor.lastIndexOf(":");
		String filename = descriptor.substring(1, sep);
		String colDesc = descriptor.substring(sep+1);
		return new String[] { filename, colDesc };
	}

	public static String encode(String filename, int colIdx, boolean allNumeral) {
		String type = (allNumeral? "n" : "s");
		String suffix = (colIdx == 0 ? type + "+" : colIdx + type);
		return "!" + filename + ":" + suffix;
	}
	
	/**
	 * 
	 * @param paramDesc optional parameter Descriptor; if null, the
	 * 		generated phrase will refer to a 'set', rather than a 'param'.
	 * @param idxDescs
	 * @return
	 */
	public static String
	getReadPhrase(String paramDesc, String... idxDescs) {
		if (idxDescs.length == 0)
			throw new IllegalArgumentException("Missing idxDescs");
		String filename = null;
		String paramColDesc = "";
		if (paramDesc != null) {
			String[] splitParamDesc = decode(paramDesc);
			filename = splitParamDesc[0];
			paramColDesc = " " + splitParamDesc[1];
		}
		List<String> argColDescs = new ArrayList<>();
		for (String it : idxDescs) {
			String[] splitIdxDesc = decode(it);
			if (filename == null)
				filename = splitIdxDesc[0];
			if (!filename.equals(splitIdxDesc[0])) {
					String msg = "Filenames are expected to be equal: " +
						"'" + filename + "' and '" + splitIdxDesc[0]+ "'";
				throw new IllegalArgumentException(msg);
			}
			argColDescs.add(splitIdxDesc[1]);
		}
		String template = "<" + String.join(",", argColDescs) + ">" +
				paramColDesc;
		int skip = template.matches("<[sn]\\+>")? 0 : 1;
		String result = getReadPhrase(filename, template, skip);
		return result;
	}
	
	public static String
	getReadPhrase(String filename, String template, int skip) {
		String skipStr = (skip == 0)? "" : "skip " + skip + " ";
		return String.format("read \"%s\" as \"%s\" %scomment \"#\"",
				filename, template, skipStr);
	}
	
	public static boolean areAllNumeral(Collection<String> elems) {
		return elems.stream().allMatch( it -> {
			try { Double.parseDouble(it); }
			catch (NumberFormatException nfe) { return false; }
			return true;
		});	
	}
	
	
	
	public static List<Loader<String>> knownLoaders =
			List.of(new CsvLoader(), new XlsxLoader(), new DotLoader(),
					new PropertiesLoader()
		);
	
	public AST resolveFromFilename(String filename) throws IOException {
		return resolveFromFilename(filename, knownLoaders);
	}
	
	public AST
	resolveFromFilename(String filename, List<Loader<String>> loaders)
			throws IOException {
		DataSourceFactory<String> dsf = new DataSourceFactory<>(loaders);
		String modelname = extractModelnameFrom(filename);
		// NOTE: if different names are used for the same model, the
		// DataSourceFactory can be reused.
		Map<String,String> ds = dsf.createFor(modelname);
		// TODO: aliasing is also used in Reporter; maybe some kind of
		// global "context" object is needed to avoid code duplication
		// and innefficient multiple loading of the same resources.
		Properties alias = new Properties();
		Path aliasPath = Paths.get("alias.properties");
		if (Files.isRegularFile(aliasPath))
			try (Reader reader = Files.newBufferedReader(aliasPath)) {
				alias.load(reader);
			}
//		System.out.println("alias: " + alias); // debug
//		System.out.println("props: " + props); // debug
//		List<Visitor<?>> visitors = Arrays.asList(
//				new ResolverVisitor(props, alias),
//				new ZimplVisitor()
//			);
//		return Visitors.reduceFromFilename(filename, visitors);
		Visitor<AST> it = new ResolverVisitor(ds, alias);
		return Visitors.reduceFromFilename(filename, List.of(it));
	}
	
	String extractModelnameFrom(String filename) {
		Path path = Paths.get(filename);
		String result = path.getFileName().toString();
		if (result.contains("."))
			result = result.substring(0, result.lastIndexOf('.'));
		return result;
	}
	
}