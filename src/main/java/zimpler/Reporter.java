package zimpler;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import tree.AST;
import tree.OpNode;
import tree.VarDecl;
import tree.VarRef;

/**
 * @author Javier MV
 * @since 0.5
 */
public class Reporter {
	
	Properties alias;
	Properties singular;
	
	public Reporter() throws IOException {
		// TODO: aliasing is also used in Resolver; maybe some kind of
		// global "context" object is needed to avoid code duplication
		// and innefficient multiple loading of the same resources.
		alias = new Properties();
		Path aliasPath = Paths.get("alias.properties");
		if (Files.isRegularFile(aliasPath))
			try (Reader reader = Files.newBufferedReader(aliasPath)) {
				alias.load(reader);
			}
		singular = new Properties();
		Path singularPath = Paths.get("singular.properties");
		if (Files.isRegularFile(singularPath))
			try (Reader reader = Files.newBufferedReader(singularPath)) {
				singular.load(reader);
			}
	}
	
	public List<Table<Object>>
	createReport(AST model, Map<String, Double> sol) {
//		System.out.println("Reporter.createReport()"); // debug
		Map<String, VarDecl> decls =
				model.accept(new VarCollectorVisitor()).stream()
				.collect(Collectors.toMap(VarDecl::getName, it -> it));
		Map<String, Table.Builder<Object>> builders = new HashMap<>();
		for (Map.Entry<String, Double> e : sol.entrySet()) {
//			System.out.println(e.getKey() + ": " + e.getValue()); // debug
			// remove from '[' to ']', keeping the name
			String varName = e.getKey().replaceAll("\\[.+?\\]", "");
			// remove what's outside of '[' & ']', keeping the content
			String indexName = e.getKey().replaceAll(".*?\\[|\\]", "");
			if (indexName.equals("")) {
				// Unindexed vars are grouped into the Summary
				indexName = varName;
				varName = "Summary";
			}
//			System.out.println("varname: " + varname);
			// varNames begining with "#" are not actual model vars;
			// they were added with solving metrics such as gap.
			if (varName.startsWith("#")) {
				indexName = varName.substring(1); // skips 1st char
				varName = "Summary";
			}
			VarDecl decl = decls.get(varName);
			assert decl != null || varName.equals("Summary");
			Table.Builder<Object> builder = builders.get(varName);
			if (builder == null) {
				builder = new Table.Builder<>(alias(varName));
				builders.put(varName, builder);
				if (decl != null && decl.getIndex() == null)
					throw new RuntimeException("unexpected condition");
				String[] columns = createColumnNamesFrom(decl);
				builder.addRow(columns);
				builder.setFirstRowAsHeader();
			}
			Object value = e.getValue();
			if (varName.equals("Summary")) {
				int colon = indexName.indexOf(':');
				if (0 < colon) {
					value = indexName.substring(colon + 1);
					indexName = indexName.substring(0, colon);
				}
			} else if (decl.getType().equals("binary") &&
					decl.getIndex() instanceof VarRef) // single elem, eg 'w[C]'
			{
				if (((Double) value).intValue() == 1)
					builder.addRow(new Object[] { indexName });
				continue;
			}
			String[] splitIndexes = indexName.split(",");
			if (varName.equals("Summary") && splitIndexes.length != 1)
				throw new RuntimeException("Unexpected, this is");
			Object[] items = Arrays.copyOf(splitIndexes,
					splitIndexes.length + 1, Object[].class);
			items[items.length-1] = value;
			builder.addRow(items);
		}
		List<Table<Object>> result = builders.entrySet().stream()
				.map(e->e.getValue().getResult()).toList();
		return result;
	}
	String alias(String name) {
		return singular(alias.getProperty(name, name));
	}
	
	String[] createColumnNamesFrom(VarDecl decl) {
		if (decl == null) // Summary
			return new String[] { "key", "value" };
		AST index = decl.getIndex();
//		System.out.println("name: " + decl.getName() + ", type: " +
//				decl.getType() + ", index: " + decl.getIndex());
		if (index instanceof VarRef) // single elem, eg 'C'
			// TODO: reporting of binary zero values should be configurable
			return decl.getType().equals("binary")?
					new String[] { alias(index.toString()) } :
					new String[] { alias(index.toString()), "value" };
		if (index instanceof OpNode && index.toString().equals("*")) {
			String[] result = new String[index.getChildCount() + 1];
			for (int i = 0; i < index.getChildCount(); ++i)
				result[i] = alias(index.getChild(i).toString());
			result[index.getChildCount()] = "value";
			return result;
		}
		// FIXME: consider '+'
		String msg = "Unexpected type: " + index.getClass();
		throw new IllegalArgumentException(msg);
	}
	
	String singular(String maybePlural) {
		String result = singular.getProperty(maybePlural);
		if (result != null)
			return result;
		return maybePlural.endsWith("s") ?
			maybePlural.substring(0, maybePlural.length()-1) :
			maybePlural;
	}
	
}
