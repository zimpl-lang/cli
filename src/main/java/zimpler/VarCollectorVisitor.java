package zimpler;

import java.util.HashSet;
import java.util.Set;

import tree.AbstractVisitor;
import tree.VarDecl;
import tree.Visitor;

/**
 * @author Javier MV
 * @since 0.5
 */
public class VarCollectorVisitor extends AbstractVisitor<Set<VarDecl>> 
implements Visitor<Set<VarDecl>> {
	
	@Override public Set<VarDecl> visitVarDecl(VarDecl node) {
		return new HashSet<>(Set.of(node));
	}
	
	@Override public Set<VarDecl>
	reduce(Set<VarDecl> current, Set<VarDecl> next, int i) {
		if (next == null)
			return current;
		if (current == null)
			return next;
		current.addAll(next);
		return current;
	}
	
}
