package zimpler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import tree.AST;
import zimpler.io.CbcSolLoader;
import zimpler.io.DataSourceFactory;
import zimpler.io.Loader;
import zimpler.io.TextFileWriter;
import zimpler.io.ZimplVisitor;

/**
 * Commnand Line Interface Optimizer. It delegates optimization to an
 * external command, expected to be found as {@code solve.bat} on
 * Windows or {@code ./solve.sh} on Linux and Mac. They are called with
 * a single argument: the location of a temporary work directory where
 * a resolved model can be found as {@code resolved-model.zimpl}.
 * 
 * @author Javier MV
 * @since 0.5
 */
public class CliOptimizer implements Optimizer {
	
	// This list holds the instances capable of reading results from
	// actual solvers (aka Optimizers). When the time comes, solution
	// loaders will be paired with their corresponding solver.
	public static List<Loader<Double>> knownLoaders =
			List.of(new CbcSolLoader());
	
	@Override
	public Map<String, Double> optimize(AST model) throws IOException {
		Path tmpDir = Files.createTempDirectory("zimpler-");
//		System.out.println("tmpDir:" + tmpDir); // debug
//		tmpDir:/tmp/zimpler-5992969351635114482
		Path modelPath = tmpDir.resolve("model.zimpl");
		String modelText = model.accept(new ZimplVisitor());
		TextFileWriter.write(modelPath, modelText);
		String[] cmdln = createCmdln(tmpDir);
		try {
			ProcessBuilder pb = new ProcessBuilder(cmdln);
			// In the reference implementation, logging of the command,
			// arguments, directory, stack trace, and process id can be
			// enabled. The logged information may contain sensitive
			// security information and the potential exposure of the
			// information should be carefully reviewed. Logging of the
			// information is enabled when the logging level of the
			// system logger named java.lang.ProcessBuilder is
			// Level.DEBUG or Level.TRACE. When enabled for Level.DEBUG
			// only the process id, directory, command, and stack trace
			// are logged. When enabled for Level.TRACE the arguments
			// are included with the process id, directory, command,
			// and stack trace.
			
			pb.redirectError(Redirect.INHERIT);
			pb.redirectOutput(Redirect.INHERIT);
			int exitCode = pb.start().waitFor();
			Path lpPath = tmpDir.resolve("model.lp");
			if (!Files.exists(lpPath)) {
				String msg = "CliOptimizer cannot solve model. " +
						"LP model not found.";
				throw new IllegalStateException(msg);
			}
			if (exitCode != 0) {
				String msg = "CliOptimizer cannot solve model.";
				throw new IllegalStateException(msg);
			}
		} catch (InterruptedException | IOException e) {
			// Does not deleteRecursively(tmpDir);
			// TODO: handle exception
			throw new RuntimeException(e);
		}
		// TODO: get console from execution; helps debugging. Maybe use
		// ProcessBuilder, maybe Process.get{Error,Output}Stream()
//		System.out.println("user.dir: " + System.getProperty("user.dir"));
		
		// TODO: evaluate if Main.filename should rather be part of the
		// model; it may be used for .lp or .mps naming, as well as a
		// hint pointing to solution's filename.
		// NOTE: current strategy: load hardcoded (base) name 'model';
		// possible next strategy: see if only one *.ext (for 'ext' a
		// recognizable extension) is found, in case 'model.ext' is
		// not present; alternative, see inmediately previous 'to do' note.
		DataSourceFactory<Double> dsf =
				new DataSourceFactory<>(knownLoaders);
		String basename = tmpDir.resolve("model").toAbsolutePath()
				.normalize().toString();
		Map<String, Double> result = dsf.createFor(basename);
		deleteRecursively(tmpDir);
		return result;
	}
	
	static void extractResource(String src, Path dst) {
		try (InputStream in =
				CliOptimizer.class.getResourceAsStream(src)) {
			if (in == null) {
				String msg = "CliOptimizer cannot solve model. " +
						"Resource " + src + " not found.";
				throw new IllegalStateException(msg);
			}
			Files.copy(in, dst); // copy (from jar) to local filesystem
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	String[] createCmdln(Path workdir) {
		String os = System.getProperty("os.name"); // Linux, Mac OS X, Windows 1?
//		System.out.println("os: " + os); // debug
		String scriptname = "solve"; // TODO: enable config of script name
		if (os.contains("Win"))
			scriptname += ".bat";
		else
			scriptname = "./" + scriptname + ".sh";
		Path scriptpath = Paths.get(scriptname);
		if (!Files.exists(scriptpath)) {
			scriptpath = workdir.resolve(scriptpath);
			// log("local scriptfile not present, getting resource")
			extractResource("/" + scriptname, scriptpath);
			scriptpath.toFile().setExecutable(true);
			scriptname = scriptpath.toString();
		}
		if (Files.isReadable(scriptpath) &&
				!Files.isExecutable(scriptpath)) {
			String msg = "CliOptimizer cannot solve model. Script " +
					scriptpath.toAbsolutePath().normalize() +
					" found but lacks execution permission.";
			throw new IllegalStateException(msg);
		}
		if (os.contains("Win"))
			// for .bat see https://stackoverflow.com/a/616014
			return new String[] { "cmd", "/c", scriptname, workdir.toString() };
		else
			return new String[] { scriptname, workdir.toString() };
	}
	
	void deleteRecursively(Path p) throws IOException {
		try (Stream<Path> paths = Files.walk(p)) {
			// Place regular files before directories
			paths.sorted(Comparator.reverseOrder())
				.map(Path::toFile).forEach(File::delete);
		}
	}
	
	@Override public boolean isEnabled() { return true; }
	
	@Override
	public String getDescription() {
		return "Command Line Interface (CLI) Optimizer";
	}
	
}
