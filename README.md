`zimpler` - Main
================

Description
-----------
`zimpler` is a tool to streamline Zimpl modeling. Introductory, Installation and General information can be found at https://gitlab.com/zimpler-tool/.

This is the _Main_ component of the `zimpler` tool; development happens here.

This project is responsible for:
- implementing core capabilities;
- defining an extension mechanisms;
- describing Command Line Interface (CLI) options; 
- handling error reporting. 

The latest stable version of this tool is focused on a single capability: **resolving** Zimpl models. The current development version adds new capabilities: **solving** (not to be confused with REsolving) and **reporting** results.

<!--- ## Integrate with your tools
- [ ] [Set up project integrations](https://gitlab.com/zimpl-lang/cli/-/settings/integrations)
--->

<!--- ## Test and Deploy
Use the built-in continuous integration in GitLab.
- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
-->

<!---
## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.
-->


Usage
-----
### Basic
<!-- Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README. -->

After [installation](https://gitlab.com/zimpler-tool/gitlab-profile/blob/main/README.md#installation), type `zimpler` on the command line to get usage information:
```console
$ zimpler
Usage: zimpler [-hV] [-o=<file>] <model>
Zimpl Expression Resolver. See https://gitlab.com/zimpler-tool for info.
      <model>           Zimpl input model file.
  -h, --help            Show this help message and exit.
  -o, --output=<file>   Place the resolved model into <file>.
  -V, --version         Print version information and exit.
Input data filename extensions:
        .xlsx  Excel 2007+ spreadsheet
  .properties  Text file with string key-value pairs
         .dot  Graphviz's DOT graph format
         .csv  Comma-separated values
```

> **NOTE:** Excel files are recognized starting from version 0.5, currently in development.

Zimpl models are read from `.zpl` files. If no input model is specified, `model.zpl` is assumed. Data files are recognized by their extension. Files sharing the model base name (without the `.zpl` extension) and with a recognizable filename extension are considered input data sources. For example, if `model.zpl`, `model.xlsx` and `model.properties` exist in the working directory, typing `zimpler` will resolve the model without need to specify any further. If no output file is declared, the resulting model will be sent to standard output.

An introductory tutorial with complete examples has been published in Section 5 from the [Sept-2024 Issue](https://ifors.org/newsletter/ifors-news-sept-2024.pdf#page=5) of the [IFORS Newsletter](https://www.ifors.org/ifors-newsletter/).

### Alias
Sometimes, mathematical problems like Graph Coloring or the Knapsack problem model practical situations. Their formulations often name generic elements, such as a graph $g = (V, E)$ or a set $I$ of _items_, but their practical applications usually name very specific elements. For example, data related to a classroom assignment problem may include Rooms and Courses, with overlapping Conflicts, recorded as different Sheets in an Excel file. In order not to force either the model, nor the data, to change, just to bind them, an `alias.properties` file can link the different identifiers together.

With `graph-coloring.zimpl` declaring `set V` and `set E`, representing conflict graph $g = (V, E)$, and `set C` as the set of available colors; Sheets _Courses_, _Rooms_ and _Conflicts_ in `graph-coloring.xlsx`,

| ![Sheet Courses](src/img/classroom-assignment-courses.png "Sheet 'Courses' from graph-coloring.xlsx") | ![Sheet Rooms](src/img/classroom-assignment-rooms.png "Sheet 'Rooms' from graph-coloring.xlsx") | ![Sheet Conflicts](src/img/classroom-assignment-conflicts.png "Sheet 'Conflicts' from graph-coloring.xlsx") |
| :-----: | :---: | :-------: |
| _Courses_ | _Rooms_ | _Conflicts_ |

and file `alias.properties` containing
~~~
V = Courses
E = Conflicts
C = Rooms
~~~
running `zimpler graph-coloring.zimpl` generates a `.csv` file for each spreadsheet and yields
~~~
set V := { read "graph-coloring-Courses.csv" as "<1s>" ... };
set E := { read "graph-coloring-Conflicts.csv" as "<1s,2s>" ... };
set C := { read "graph-coloring-Rooms.csv" as "<1s>" ... };
~~~
for the resolved extract of `graph-coloring.zimpl`.

> **NOTE:** for resolution to take a single column from an input sheet or table, as with `V` and `C` in the example, a header "_Name_" is currently required as a column name; otherwise, the complete table is resolved, as is the case for `E` in the same example.

### Solving & Reporting
The current development version provides an early implementation of both solving and reporting capabilities. After resolution, they run silently and automatically, generating a report as Excel file `result.xlsx` with a _Summary_ sheet and additional sheets for each set of variables. For the example in the [Alias Section](#alias), the generated report is

| ![Sheet Summary](src/img/classroom-assignment-report-summary.png "Sheet 'Summary' from result.xlsx") | ![Sheet Assignment](src/img/classroom-assignment-report-assignment.png "Sheet 'Assignment' from result.xlsx") | ![Sheet Usage](src/img/classroom-assignment-report-usage.png "Sheet 'Usage' from result.xlsx") |
| :-----: | :---: | :-------: |
| _Summary_ | _Assignment_ | _Usage_ |

with the following definitions added to file `alias.properties`
~~~
w = Usage
x = Assignment
...
~~~
Notice that _alias_ information is used to translate `var w[C]` and `var x[V*C]` from `graph-coloring.zimpl` into sheet names _Assignment_ and _Usage_. In addition, column names _Course_ and _Room_ are now in singular form, naively adapted from their plurals in `alias.properties`. If simple strategy _remove the trailing 's'_ fails, use file `singular.properties` to this end; eg:
~~~
Octopi = Octopus
Octopuses = Octopus
~~~

This preliminar implementation delegates to the [Zimpl tool](https://zimpl.zib.de/) the translation from the fully resolved `.zpl` model to the concrete `.lp` file sent to the solver. It also delegates solving to [CBC](https://github.com/coin-or/Cbc). Execution is coordinated through script `solve.sh` on Linux and Mac and `solve.bat` on Windows. For solving and reporting to work, both Zimpl and CBC are currently required, as well as a coordinating script such as [this](src/main/resources/solve.sh).

> **NOTE:** expect solving and reporting to fail often, both silently and loudly, until further stabilized. 


<!---
## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.
-->

<!-- ## Authors and acknowledgment -->
<!--- Show your appreciation to those who have contributed to the project. -->
<!-- This project is not a derivative, nor is affiliated, with the original [Zimpl](https://zimpl.zib.de/) tool. -->


License
-------
`zimpler` is free to use and share under the [GNU General Public License (GPL) v3.0](LICENSE).

<!--
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->


Building
--------
To create your own runnable version, there are hopefully friendly and complete [BUILD](src/build/BUILD.md) instructions available.


Dependencies
------------
This project depends on `zimpl-parser.jar` from [Zimpl ANTLR parser](https://gitlab.com/antlr-java-parser/zimpl), the [Antlr4](https://www.antlr.org) runtime, and [picocli](https://picocli.info/) for Command Line Interface parameters handling.

```mermaid
---
title: dependencies
---
%% for syntax, see http://mermaid.js.org/syntax/flowchart.html
graph TD
zimpler([zimpler])
zimpler -.-> parser(["zimpl-parser-0.4.jar 🫙"])
zimpler -.-> antlr(["antlr4-runtime-4.13.jar 🫙"])
zimpler -.-> picocli(["picocli-4.7.jar 🫙"])

click parser "https://gitlab.com/antlr-java-parser/zimpl" "Zimpl parser"
click picocli "https://picocli.info/"
click antlr "https://www.antlr.org/"
```

Integrations require additional dependencies; put them all in the `./lib` folder and file `MANIFEST.MF` inside `zimpler.jar` takes care of the Java class-path.

Native image generation additionally requires `picocli-codegen` and [GraalVM](https://www.graalvm.org/).

At some point in time, [StringTemplate](https://www.stringtemplate.org/) (ST) was required. The current (4.3.4) version of ST is not compatible with antlr4-runtime-4.13, presumably an antlr3 issue; therefore, older `ST-4.0.8-complete.jar` (with an embedded antlr3-runtime) was used for development, the same version that's included in `antlr-4.13.*-complete.jar` ("the tool itself"). Future versions will remove this dependency.

<!-- Nevertheless, to smooth out the current testing and development, Zimpl generation is embedded, also requiring [StringTemplate](https://www.stringtemplate.org/) (ST). The current (4.3.4) version of ST is not compatible with antlr4-runtime-4.13, presumably an antlr3 issue; therefore, `ST-4.0.8-complete.jar` (with an embedded antlr3-runtime) is used for development, the same version that's inclu<ed in `antlr-4.13.*-complete.jar` ("the tool itself"). -->
